<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "Admin" middleware group. Enjoy building your Admin!
|
*/
Route::get('app/lang', 'HomeController@lang');


/*
|--------------------------------------------------------------------------
| Admin Auth
|--------------------------------------------------------------------------
| Here is where admin auth routes exists for login and log out
*/
Route::group([
    'namespace'  => 'Auth',
], function() {
    Route::get('login', ['uses' => 'LoginController@showLoginForm','as'=>'admin.login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::group([
        'middleware' => 'App\Http\Middleware\RedirectIfAuthenticatedAdmin',
    ], function() {
        Route::post('logout', ['uses' => 'LoginController@logout','as'=>'admin.logout']);
    });
});
/*
|--------------------------------------------------------------------------
| Admin After login in
|--------------------------------------------------------------------------
| Here is where admin panel routes exists after login in
*/
Route::group([
    'middleware'  => ['App\Http\Middleware\RedirectIfAuthenticatedAdmin'],
], function() {
    Route::get('/', 'HomeController@index');
    Route::post('notification/send', 'HomeController@general_notification');

    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'app_managements',
        'namespace'=>'AppManagement',
    ],function () {
        Route::group([
            'prefix'=>'admins'
        ],function () {
            Route::get('/','AdminController@index');
            Route::get('/create','AdminController@create');
            Route::post('/','AdminController@store');
            Route::get('/{admin}','AdminController@show');
            Route::get('/{admin}/edit','AdminController@edit');
            Route::put('/{admin}','AdminController@update');
            Route::delete('/{admin}','AdminController@destroy');
            Route::patch('/update/password',  'AdminController@updatePassword');
            Route::get('/option/export','AdminController@export');
            Route::get('/{id}/activation','AdminController@activation');
        });
        Route::group([
            'prefix'=>'roles'
        ],function () {
            Route::get('/','RoleController@index');
            Route::get('/create','RoleController@create');
            Route::post('/','RoleController@store');
            Route::get('/{role}','RoleController@show');
            Route::get('/{role}/edit','RoleController@edit');
            Route::put('/{role}','RoleController@update');
            Route::delete('/{role}','RoleController@destroy');
            Route::get('/option/export','RoleController@export');
        });
        Route::group([
            'prefix'=>'permissions'
        ],function () {
            Route::get('/','PermissionController@index');
            Route::get('/create','PermissionController@create');
            Route::post('/','PermissionController@store');
            Route::get('/{permission}','PermissionController@show');
            Route::get('/{permission}/edit','PermissionController@edit');
            Route::put('/{permission}','PermissionController@update');
            Route::delete('/{permission}','PermissionController@destroy');
            Route::get('/option/export','PermissionController@export');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Data
    |--------------------------------------------------------------------------
    | Here is where App Data routes
    */
    Route::group([
        'prefix'=>'app_data',
        'namespace'=>'AppData',
    ],function () {
        Route::group([
            'prefix'=>'settings'
        ],function () {
            Route::get('/','SettingController@index');
            Route::get('/{setting}/edit','SettingController@edit');
            Route::put('/{setting}','SettingController@update');
        });
        Route::group([
            'prefix'=>'categories'
        ],function () {
            Route::get('/','CategoryController@index');
            Route::get('/create','CategoryController@create');
            Route::post('/','CategoryController@store');
            Route::get('/{category}','CategoryController@show');
            Route::get('/{category}/edit','CategoryController@edit');
            Route::put('/{category}','CategoryController@update');
            Route::get('/option/export','CategoryController@export');
            Route::get('/{id}/activation','CategoryController@activation');
        });
        Route::group([
            'prefix'=>'times'
        ],function () {
            Route::get('/','TimeController@index');
            Route::get('/create','TimeController@create');
            Route::post('/','TimeController@store');
            Route::get('/{time}','TimeController@show');
            Route::get('/{time}/edit','TimeController@edit');
            Route::put('/{time}','TimeController@update');
            Route::get('/{id}/activation','TimeController@activation');
        });
        Route::group([
            'prefix'=>'faqs'
        ],function () {
            Route::get('/','FaqController@index');
            Route::get('/create','FaqController@create');
            Route::post('/','FaqController@store');
            Route::get('/{faq}','FaqController@show');
            Route::get('/{faq}/edit','FaqController@edit');
            Route::put('/{faq}','FaqController@update');
            Route::delete('/{faq}','FaqController@destroy');
            Route::get('/option/export','FaqController@export');
            Route::get('/{id}/activation','FaqController@activation');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Data
    |--------------------------------------------------------------------------
    | Here is where App Data routes
    */
    Route::group([
        'prefix'=>'app_posts',
        'namespace'=>'AppPost',
    ],function () {
        Route::group([
            'prefix'=>'articles'
        ],function () {
            Route::get('/','ArticleController@index');
            Route::get('/create','ArticleController@create');
            Route::post('/','ArticleController@store');
            Route::get('/{article}','ArticleController@show');
            Route::get('/{article}/edit','ArticleController@edit');
            Route::put('/{article}','ArticleController@update');
            Route::get('/option/export','ArticleController@export');
            Route::get('/{id}/activation','ArticleController@activation');
        });
        Route::group([
            'prefix'=>'questions'
        ],function () {
            Route::get('/','QuestionController@index');
            Route::get('/create','QuestionController@create');
            Route::post('/','QuestionController@store');
            Route::get('/{question}','QuestionController@show');
            Route::get('/{question}/edit','QuestionController@edit');
            Route::put('/{question}','QuestionController@update');
            Route::get('/option/export','QuestionController@export');
            Route::get('/{id}/activation','QuestionController@activation');
        });
        Route::group([
            'prefix'=>'videos'
        ],function () {
            Route::get('/','VideoController@index');
            Route::get('/create','VideoController@create');
            Route::post('/','VideoController@store');
            Route::get('/{video}','VideoController@show');
            Route::get('/{video}/edit','VideoController@edit');
            Route::put('/{video}','VideoController@update');
            Route::get('/option/export','VideoController@export');
            Route::get('/{id}/activation','VideoController@activation');
        });
    });
    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'user_managements',
        'namespace'=>'UserManagement',
    ],function () {
        Route::group([
            'prefix'=>'users'
        ],function () {
            Route::get('/','UserController@index');
            Route::get('/{user}','UserController@show');
            Route::patch('/update/password',  'UserController@updatePassword');
            Route::get('/option/export','UserController@export');
            Route::get('/{id}/activation','UserController@activation');
        });
        Route::group([
            'prefix'=>'lawyers'
        ],function () {
            Route::get('/','LawyerController@index');
            Route::get('/{lawyer}','LawyerController@show');
            Route::patch('/update/password',  'LawyerController@updatePassword');
            Route::get('/option/export','LawyerController@export');
            Route::get('/{id}/activation','LawyerController@activation');
        });
        Route::group([
            'prefix'=>'tickets'
        ],function () {
            Route::get('/','TicketController@index');
            Route::get('/{ticket}','TicketController@show');
            Route::post('/{ticket}/response','TicketController@response');
            Route::get('/{ticket}/close','TicketController@close');
            Route::get('/{ticket}/reopen','TicketController@reopen');
        });
    });

    /*
    |--------------------------------------------------------------------------
    | Admin > App Management
    |--------------------------------------------------------------------------
    | Here is where App Management routes
    */

    Route::group([
        'prefix'=>'order_managements',
        'namespace'=>'OrderManagement',
    ],function () {
        Route::group([
            'prefix'=>'orders'
        ],function () {
            Route::get('/','OrderController@index');
            Route::get('/{order}','UserController@show');
            Route::get('/option/export','OrderController@export');
        });

    });

});
