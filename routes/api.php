<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login','AuthController@login');
    Route::post('signup','AuthController@register');
    Route::post('forget_password','AuthController@forget_password');
    Route::post('reset_password','AuthController@reset_password');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('me','AuthController@show');
        Route::post('refresh','AuthController@refresh');
        Route::post('update','AuthController@update');
        Route::get('resend_verify', 'AuthController@resend_verify');
        Route::post('verify', 'AuthController@verify');
        Route::post('change_password','AuthController@change_password');
        Route::post('logout','AuthController@logout');
    });
});
Route::group([
    'middleware' => 'auth:api'
], function() {
    Route::group([
        'prefix' => 'notifications',
    ], function() {
        Route::get('/', 'NotificationController@index');
        Route::post('/read', 'NotificationController@read');
        Route::post('/read/all', 'NotificationController@read_all');
    });
    Route::group([
        'prefix' => 'lawyer',
    ], function() {
        Route::get('/me', 'LawyerController@me');
        Route::post('/update', 'LawyerController@update');
        Route::post('/request_withdraw', 'LawyerController@request_withdraw');
    });
    Route::group([
        'prefix' => 'user',
    ], function() {
        Route::get('/my_interests', 'UserController@my_interests');
        Route::get('/my_files', 'UserController@my_files');
        Route::post('/update_interests', 'UserController@update_interests');
    });
    Route::group([
        'prefix' => 'posts',
    ], function() {
        Route::get('/articles', 'PostController@articles');
        Route::get('/questions', 'PostController@questions');
        Route::get('/videos', 'PostController@videos');
        Route::get('/articles/show', 'PostController@show_article');
        Route::get('/questions/show', 'PostController@show_question');
        Route::get('/videos/show', 'PostController@show_video');
    });
    Route::group([
        'prefix' => 'orders',
    ], function() {
        Route::get('/', 'OrderController@index');
        Route::get('/list', 'OrderController@list');
        Route::get('/show', 'OrderController@show');
        Route::get('/room', 'OrderController@room');
        Route::post('/', 'OrderController@store');
        Route::get('/lawyers', 'OrderController@getLawyers');
        Route::get('/offers', 'OrderController@getOffers');
        Route::post('/offer', 'OrderController@postOffer');
        Route::post('/update', 'OrderController@update');
        Route::post('/review', 'OrderController@review');
        Route::post('/upload', 'OrderController@upload_image');
        Route::post('/send_notification', 'OrderController@send_notification');
        Route::post('/request_complete', 'OrderController@request_complete');
        Route::post('/delete_media', 'OrderController@delete_media');
    });
    Route::group([
        'prefix' => 'transactions',
    ], function() {
        Route::get('/', 'TransactionController@index');
        Route::get('/my_balance', 'TransactionController@my_balance');
        Route::post('/generate_checkout', 'TransactionController@generate_checkout');
        Route::get('/check_payment', 'TransactionController@check_payment');
    });
    Route::post('tickets/add','HomeController@addTicket');
    Route::post('/add/response','HomeController@add_response');
    Route::get('tickets','HomeController@tickets');
    Route::get('tickets/show','HomeController@show_ticket');
});
Route::get('faqs','HomeController@faqs');
Route::get('install','HomeController@install');

