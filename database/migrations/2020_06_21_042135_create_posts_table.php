<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('added_by')->nullable();
            $table->string('title');
            $table->text('summery')->nullable();
            $table->longText('description')->nullable();
            $table->string('title_ar');
            $table->text('summery_ar')->nullable();
            $table->longText('description_ar')->nullable();
            $table->string('file')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
