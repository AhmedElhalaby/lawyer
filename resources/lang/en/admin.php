<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'sidebar'=>[
        'home'=>'Home',
        'app_managements'=>'App Managements',
        'user_managements'=>'Users Management',
        'order_managements'=>'Consultants Management',
        'app_data'=>'App Data',
        'faqs'=>'FAQ',
        'app_posts'=>'App Posts',
        'admins'=>'Admins',
        'users'=>'Users',
        'lawyers'=>'Lawyers',
        'tickets'=>'Tickets',
        'orders'=>'Consultants',
        'settings'=>'Settings',
        'categories'=>'Categories',
        'articles'=>'Articles',
        'questions'=>'Questions',
        'videos'=>'Videos',
        'roles'=>'Roles',
        'permissions'=>'Permissions',
        'times'=>'Available Times',
        'app_settings'=>'App Settings',
        'app_users'=>'App Users',
        'customer_management'=>'Customer Management',
        'customers'=>'Customers',
    ],
    'Home'=>[
        'n_send_general'=>'Send Public Notification ',
        'n_title'=>'Notification Title',
        'n_text'=>'Notification Text',
        'n_enter_title'=>'Enter Notification Title',
        'n_enter_text'=>'Enter Notification Text',
        'n_send'=>'Send',
        'n_type'=>'Object',
        'n_type_0'=>'All',
        'n_type_1'=>'Users',
        'n_type_2'=>'Providers',
        'total_profit'=>'Total Profit',
        'total_payment_required'=>'Lawyer Payment',
    ],
/////////////////////// Most Used ////////////////////////
    'print' => 'Print',
    'pdf' => 'PDF',
    'excel' => 'Excel',
    'sure_to_delete' => 'Are You Sure You Want To Delete',
    'sure_to_finish' => 'Are You Sure You Want To Finish ',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'close' => 'Close',
    'reopen' => 'Reopen',
    'export' => 'Export',
    'add' => 'Add',
    'change_password' => 'Edit Password',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'show' => 'Show',
    'search' => 'Search',
    'advance_search' => 'Advance Search',
    'switch_lang' => 'Switch Language',
    'exported_by' => 'Exported By',
    'exported_date' => 'Exported Date',
    'minute' => 'Minutes',
    'the_minute' => 'Minute',
    'finish' => 'Finish',

/////////////////////// Response Messages ////////////////////////
    'messages'=>[
        'deleted_successfully'=>'Deleted Successfully !',
        'saved_successfully'=>'Saved Successfully !',
        'wrong_data'=>'Wrong Data !',
        'your_account_is_in_active'=>'Your Account is in-active !',
        'you_cant_deactivate_your_account'=>'You Can`nt de-activate your account  !',
        'you_cant_delete_your_account'=>'You Can`nt delete your account  !',
        'notification_sent'=>'Notification Sent Successfully !',
    ],
    'activation'=>[
        'active' => 'Active',
        'do_active' => 'Activate',
        'in_active' => 'In-Active',
        'do_in_active' => 'De-Activate',
    ],
    'passwords'=>[
        'password' => 'Password',
        'password_confirmation' => 'Password Confirmation',
    ],
];
