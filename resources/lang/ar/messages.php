<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'object_not_found' => 'الهدف غير موجود !',
    'deleted_successful' => 'تم الحذف بنجاح',
    'created_successful' => 'تم الاضافة بنجاح',
    'updated_successful' => 'تم الحفظ بنجاح',
    'rated_successful' => 'تم التقييم بنجاح',
    'unfav_successful' => 'تم الازالة من المفضلة',
    'fav_successful' => 'تم الاضافة للمفضلة',
    'unfollow_successful' => 'تم ازالة المتابعة بنجاح',
    'follow_successful' => 'تم المتابعة بنجاح',
    'deleted_successfully'=>'تم الحذف بنجاح !',
    'saved_successfully'=>'تم الحفظ بنجاح !',
    'canceled_successfully'=>'تم الإلغاء بنجاح !',
    'rated_successfully'=>'تم التقييم بنجاح !',
    'you_are_not_allowed'=>'غير متاح لك عرض هذا المحتوى !',
    'wrong_data'=>'الإدخال خاطئ !',
    'offer_exists'=>'لا يمكنك اضافة اكثر من عرض للهدف الواحد !',
    'payment_method_not_allowed'=>'طريقة الدفع غير متاحة !',
    'date_reserved'=>'هذا التاريخ محجوز مسبقا !',
    'offer_expired'=>'هذا العرض منتهي !',
    'wrong_sequence'=>'ترتيب خاطئ !',
    'dont_have_permission'=>'لا تمتلك صلاحيات لهذا الاجراء !',
    'not_paid_yet'=>'لم يتم دفع المبلغ بعد !',
    'dont_have_enough_credit'=>'قيمة المحفظة اقل من القيمة المطلوبة يرجى اضافة المبلغ للمحفظة !',
    'you_cannot_do_it_at_this_time'=>'لا يمكنك القيام بهذا الإجراء في الوقت الحالي !',
    'you_cannot_do_it_again'=>'لا يمكنك القيام بهذا الإجراء مرة اخرى !',
    'OrderStatus'=>[
        'New'=>[
            'title'=>'طلب جديد',
            'message'=>'لديك طلب جديد'
        ],
        'Accepted'=>[
            'title'=>'تم قبول طلبك',
            'message'=>'لقد تم قبول طلبك من قبل المزود'
        ],
        'Rejected'=>[
            'title'=>'تم رفض طلبك',
            'message'=>'لقد قام المزود برفض طلبك !'
        ],
        'Canceled'=>[
            'title'=>'تم الغاء الطلب',
            'message'=>'لقد قام الز بون بالغاء الطلب المقدم لك !'
        ],
        'Processing'=>[
            'title'=>'قيد التنفيذ',
            'message'=>'طلبك قيد التنفيذ'
        ],
        'Delivered'=>[
            'title'=>'تم وصول الطلب',
            'message'=>'لقد تم وصول الطلب للمكان المحدد'
        ],
        'Received'=>[
            'title'=>'تم استلام الطلب',
            'message'=>'لقد قمت باستلام الطلب'
        ],
    ]

];
