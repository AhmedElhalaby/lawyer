@extends('AhmedPanel.crud.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header " data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.show')}} {{__(('crud.'.$lang.'.crud_the_name'))}}</h4>
                </div>
                <div class="card-content">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.User.name')}}</th>
                                            <td style="border-top: none !important;">{{$Object->first_name .' '.$Object->last_name}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.User.mobile')}}</th>
                                            <td style="border-top: none !important;">{{$Object->mobile}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.User.email')}}</th>
                                            <td style="border-top: none !important;">{{$Object->email}}</td>
                                        </tr>

                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.User.created_at')}}</th>
                                            <td style="border-top: none !important;">{{\Carbon\Carbon::parse($Object->created_at)->format('Y-m-d')}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Order.crud_names')}}</th>
                                            <td style="border-top: none !important;"><a href="{{url('admin/order_managements/orders?lawyer_id='.$Object->lawyer->id)}}" target="_blank" class="btn btn-xs btn-primary" style="color: #fff !important;">{{\App\Models\Order::where('lawyer_id',$Object->lawyer->id)->count()}}</a></td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.User.balance')}}</th>
                                            <td style="border-top: none !important;">{{\App\Helpers\Functions::UserBalance($Object->id)}} $</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.User.is_active')}}</th>
                                            <td style="border-top: none !important;">
                                                <span class="label label-{{($Object->is_active)?'success':'danger'}}">{{($Object->is_active)?__('admin.activation.active'):__('admin.activation.in_active')}}</span>
                                            </td>
                                        </tr>

                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.union_membership')}}</th>
                                            <td style="border-top: none !important;">
                                                <a href="{{asset($Object->lawyer->union_membership)}}" class="text-primary" download >
                                                    <i class="material-icons ">cloud_download</i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.school_certificate')}}</th>
                                            <td style="border-top: none !important;">
                                                <a href="{{asset($Object->lawyer->school_certificate)}}" class="text-primary" download >
                                                    <i class="material-icons ">cloud_download</i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.practice_certificate')}}</th>
                                            <td style="border-top: none !important;">
                                                <a href="{{asset($Object->lawyer->practice_certificate)}}" class="text-primary" download >
                                                    <i class="material-icons ">cloud_download</i>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.bio')}}</th>
                                            <td style="border-top: none !important;">{{$Object->lawyer->bio}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.work_address')}}</th>
                                            <td style="border-top: none !important;">{{$Object->lawyer->work_address}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.contact_email')}}</th>
                                            <td style="border-top: none !important;">{{$Object->lawyer->contact_email}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.contact_mobile')}}</th>
                                            <td style="border-top: none !important;">{{$Object->lawyer->contact_mobile}}</td>
                                        </tr>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Lawyer.educational_level')}}</th>
                                            <td style="border-top: none !important;">{{__('crud.Lawyer.EducationalLevel.'.$Object->lawyer->educational_level)}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header text-center" style="padding: 5px" data-background-color="{{ config('app.color') }}">
                                    <h4 class="title"> {{__('crud.Transaction.crud_names')}}</h4>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th style="border-top: none !important;">{{__('crud.Transaction.type')}}</th>
                                            <th style="border-top: none !important;">{{__('crud.Transaction.value')}}</th>
                                            <th style="border-top: none !important;">{{__('crud.Transaction.order_id')}}</th>
                                            <th style="border-top: none !important;">{{__('crud.Transaction.created_at')}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($Transactions as $Transaction)
                                            <tr>
                                                <td>{{__('crud.Transaction.Types.'.$Transaction->getType())}}</td>
                                                <td>{{$Transaction->getValue()}}</td>
                                                <td>{{($Transaction->getOrderId())?'#'.$Transaction->getOrderId():'-'}}</td>
                                                <td>{{\Carbon\Carbon::parse($Transaction->created_at)->format('Y-m-d')}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header text-center" style="padding: 5px" data-background-color="{{ config('app.color') }}">
                                    <h4 class="title"> {{__('crud.Category.crud_names')}}</h4>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <tbody>
                                        @foreach($LawyerCategories as $LawyerCategory)
                                            <tr>
                                                <td class="text-center">{{$LawyerCategory->category->name}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
