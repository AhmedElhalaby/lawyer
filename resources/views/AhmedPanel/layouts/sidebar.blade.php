<li class="nav-item @if(url()->current() == url('admin')) active @endif ">
    <a href="{{url('admin')}}" class="nav-link">
        <i class="material-icons">dashboard</i>
        <p>{{__('admin.sidebar.home')}}</p>
    </a>
</li>
@if (auth('admin')->user()->can('Admins') ||auth('admin')->user()->can('Roles') ||auth('admin')->user()->can('Permissions'))
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_managements'))===0) in @endif" id="app_managements" @if(strpos(url()->current() , url('admin/app_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            @if (auth('admin')->user()->can('Admins'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/admins'))===0) active @endif">
                <a href="{{url('admin/app_managements/admins')}}" class="nav-link">
                    <i class="material-icons">group</i>
                    <p>{{__('admin.sidebar.admins')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Roles'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/roles'))===0) active @endif">
                <a href="{{url('admin/app_managements/roles')}}" class="nav-link">
                    <i class="material-icons">accessibility</i>
                    <p>{{__('admin.sidebar.roles')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Permissions'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/permissions'))===0) active @endif">
                <a href="{{url('admin/app_managements/permissions')}}" class="nav-link">
                    <i class="material-icons">vpn_key</i>
                    <p>{{__('admin.sidebar.permissions')}}</p>
                </a>
            </li>
            @endif
        </ul>
    </div>
</li>
@endif
@if (auth('admin')->user()->can('Settings') ||auth('admin')->user()->can('Categories') ||auth('admin')->user()->can('Times')||auth('admin')->user()->can('Faqs'))
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_data" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_data')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_data'))===0) in @endif" id="app_data" @if(strpos(url()->current() , url('admin/app_data'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            @if (auth('admin')->user()->can('Settings'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/settings'))===0) active @endif">
                <a href="{{url('admin/app_data/settings')}}" class="nav-link">
                    <i class="material-icons">settings</i>
                    <p>{{__('admin.sidebar.settings')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Categories'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/categories'))===0) active @endif">
                <a href="{{url('admin/app_data/categories')}}" class="nav-link">
                    <i class="material-icons">category</i>
                    <p>{{__('admin.sidebar.categories')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Times'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/times'))===0) active @endif">
                <a href="{{url('admin/app_data/times')}}" class="nav-link">
                    <i class="material-icons">access_time</i>
                    <p>{{__('admin.sidebar.times')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Faqs'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_data/faqs'))===0) active @endif">
                <a href="{{url('admin/app_data/faqs')}}" class="nav-link">
                    <i class="material-icons">help</i>
                    <p>{{__('admin.sidebar.faqs')}}</p>
                </a>
            </li>
            @endif
        </ul>
    </div>
</li>
@endif
@if (auth('admin')->user()->can('Articles') ||auth('admin')->user()->can('Questions') ||auth('admin')->user()->can('Videos'))
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_posts" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_posts')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_posts'))===0) in @endif" id="app_posts" @if(strpos(url()->current() , url('admin/app_posts'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            @if (auth('admin')->user()->can('Articles'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_posts/articles'))===0) active @endif">
                <a href="{{url('admin/app_posts/articles')}}" class="nav-link">
                    <i class="material-icons">post_add</i>
                    <p>{{__('admin.sidebar.articles')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Questions'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_posts/questions'))===0) active @endif">
                <a href="{{url('admin/app_posts/questions')}}" class="nav-link">
                    <i class="material-icons">help</i>
                    <p>{{__('admin.sidebar.questions')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Videos'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_posts/videos'))===0) active @endif">
                <a href="{{url('admin/app_posts/videos')}}" class="nav-link">
                    <i class="material-icons">videocam</i>
                    <p>{{__('admin.sidebar.videos')}}</p>
                </a>
            </li>
            @endif
        </ul>
    </div>
</li>
@endif
@if (auth('admin')->user()->can('Users') ||auth('admin')->user()->can('Lawyers') ||auth('admin')->user()->can('Tickets'))
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#user_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.user_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/user_managements'))===0) in @endif" id="user_managements" @if(strpos(url()->current() , url('admin/user_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            @if (auth('admin')->user()->can('Users'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/user_managements/users'))===0) active @endif">
                <a href="{{url('admin/user_managements/users')}}" class="nav-link">
                    <i class="material-icons">group</i>
                    <p>{{__('admin.sidebar.users')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Lawyers'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/user_managements/lawyers'))===0) active @endif">
                <a href="{{url('admin/user_managements/lawyers')}}" class="nav-link">
                    <i class="material-icons">gavel</i>
                    <p>{{__('admin.sidebar.lawyers')}}</p>
                </a>
            </li>
            @endif
            @if (auth('admin')->user()->can('Tickets'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/user_managements/tickets'))===0) active @endif">
                <a href="{{url('admin/user_managements/tickets')}}" class="nav-link">
                    <i class="material-icons">label</i>
                    <p>{{__('admin.sidebar.tickets')}}</p>
                </a>
            </li>
            @endif
        </ul>
    </div>
</li>
@endif
@if (auth('admin')->user()->can('Consultants'))
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#order_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.order_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/order_managements'))===0) in @endif" id="order_managements" @if(strpos(url()->current() , url('admin/order_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            @if (auth('admin')->user()->can('Consultants'))
            <li class="nav-item @if(strpos(url()->current() , url('admin/order_managements/orders'))===0) active @endif">
                <a href="{{url('admin/order_managements/orders')}}" class="nav-link">
                    <i class="material-icons">category</i>
                    <p>{{__('admin.sidebar.orders')}}</p>
                </a>
            </li>
            @endif
        </ul>
    </div>
</li>
@endif
