<?php

namespace App\Console\Commands;

use App\Helpers\Functions;
use Illuminate\Console\Command;

class OrderNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify Client and Lawyer with the time of the consult';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        Functions::OrderTimeNotification();
    }
}
