<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Lawyer\MeRequest;
use App\Http\Requests\Api\Lawyer\RequestWithdrawRequest;
use App\Http\Requests\Api\Lawyer\UpdateLawyerForm;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class LawyerController extends Controller
{
    use ResponseTrait;

    /**
     * @param MeRequest $form
     * @return JsonResponse
     */
    public function me(MeRequest $form)
    {
        return $form->persist();
    }
    /**
     * @param UpdateLawyerForm $form
     * @return JsonResponse
     */
    public function update(UpdateLawyerForm $form)
    {
        return $form->persist();
    }
    /**
     * @param RequestWithdrawRequest $form
     * @return JsonResponse
     */
    public function request_withdraw(RequestWithdrawRequest $form)
    {
        return $form->persist();
    }
}
