<?php

namespace App\Http\Controllers\Api;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\AddTicketRequest;
use App\Http\Requests\Api\AddTicketResponseRequest;
use App\Http\Requests\Api\FaqRequest;
use App\Http\Requests\Api\TicketRequest;
use App\Http\Requests\Api\ShowTicketRequest;
use App\Http\Resources\Api\General\CategoryResource;
use App\Models\Category;
use App\Models\Setting;
use App\Models\Time;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class HomeController extends Controller
{
    use ResponseTrait;

    /**
     * @return JsonResponse
     */
    public function install(){
        $data = [];
        $data['Settings'] = Setting::pluck((app()->getLocale() =='en')?'value':'value_ar','key')->toArray();
        $data['Categories'] = CategoryResource::collection(Category::where('is_active',true)->get());
        $data['UserTypes'] = Constant::USER_TYPE;
        $data['UserGender'] = Constant::USER_GENDER;
        $data['EducationalLevel'] = Constant::EDUCATIONAL_LEVEL();
        $data['OrderStatus'] = Constant::ORDER_STATUS;
        $data['OrderOfferStatus'] = Constant::ORDER_OFFER_STATUS;
        $data['LawyerStatus'] = Constant::LAWYER_STATUS;
        $data['TransactionTypes'] = Constant::TRANSACTION_TYPES;
        $data['TransactionStatuses'] = Constant::TRANSACTION_STATUS;
        $data['Times'] = Time::where('is_active',true)->get();
        return $this->successJsonResponse([],$data,'data');
    }

    /**
     * @param TicketRequest $request
     * @return JsonResponse
     */
    public function tickets(TicketRequest $request){
        return $request->persist();
    }
    /**
     * @param ShowTicketRequest $request
     * @return JsonResponse
     */
    public function show_ticket(ShowTicketRequest $request){
        return $request->persist();
    }
    /**
     * @param AddTicketRequest $request
     * @return JsonResponse
     */
    public function addTicket(AddTicketRequest $request){
        return $request->persist();
    }

    /**
     * @param AddTicketResponseRequest $request
     * @return JsonResponse
     */
    public function add_response(AddTicketResponseRequest $request){
        return $request->persist();
    }
    /**
     * @param FaqRequest $request
     * @return JsonResponse
     */
    public function faqs(FaqRequest $request)
    {
        return $request->persist();
    }

}
