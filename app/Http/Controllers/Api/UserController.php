<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\User\MyFilesForm;
use App\Http\Requests\Api\User\MyInterestsForm;
use App\Http\Requests\Api\User\UpdateInterestsForm;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    use ResponseTrait;

    /**
     * @param MyInterestsForm $form
     * @return JsonResponse
     */
    public function my_interests(MyInterestsForm $form)
    {
        return $form->persist();
    }
    /**
     * @param UpdateInterestsForm $form
     * @return JsonResponse
     */
    public function update_interests(UpdateInterestsForm $form)
    {
        return $form->persist();
    }
    /**
     * @param MyFilesForm $form
     * @return JsonResponse
     */
    public function my_files(MyFilesForm $form)
    {
        return $form->persist();
    }
}
