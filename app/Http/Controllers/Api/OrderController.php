<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Order\DeleteMediaRequest;
use App\Http\Requests\Api\Order\GetLawyerRequest;
use App\Http\Requests\Api\Order\GetOfferRequest;
use App\Http\Requests\Api\Order\PostOfferRequest;
use App\Http\Requests\Api\Order\RequestCompleteRequest;
use App\Http\Requests\Api\Order\RoomRequest;
use App\Http\Requests\Api\Order\SendNotificationRequest;
use App\Http\Requests\Api\Order\ShowRequest;
use App\Http\Requests\Api\Order\StoreRequest;
use App\Http\Requests\Api\Order\IndexRequest;
use App\Http\Requests\Api\Order\UpdateRequest;
use App\Http\Requests\Api\Order\UploadRequest;
use App\Http\Requests\Api\Order\ReviewRequest;
use App\Http\Requests\Api\Order\ListRequest;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    /**
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request){
        return $request->persist();
    }
    /**
     * @param RoomRequest $request
     * @return JsonResponse
     */
    public function room(RoomRequest $request){
        return $request->persist();
    }
    /**
     * @param ShowRequest $request
     * @return JsonResponse
     */
    public function show(ShowRequest $request){
        return $request->persist();
    }

    /**
     * @param GetLawyerRequest $request
     * @return JsonResponse
     */
    public function getLawyers(GetLawyerRequest $request){
        return $request->persist();
    }

    /**
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request){
        return $request->persist();
    }

    /**
     * @param GetOfferRequest $request
     * @return JsonResponse
     */
    public function getOffers(GetOfferRequest $request){
        return $request->persist();
    }

    /**
     * @param PostOfferRequest $request
     * @return JsonResponse
     */
    public function postOffer(PostOfferRequest $request){
        return $request->persist();
    }

    /**
     * @param UpdateRequest $request
     * @return JsonResponse
     */
    public function update(UpdateRequest $request){
        return $request->persist();
    }

    /**
     * @param UploadRequest $request
     * @return JsonResponse
     */
    public function upload_image(UploadRequest $request){
        return $request->persist();
    }

    /**
     * @param SendNotificationRequest $request
     * @return JsonResponse
     */
    public function send_notification(SendNotificationRequest $request){
        return $request->persist();
    }
    /**
     * @param RequestCompleteRequest $request
     * @return JsonResponse
     */
    public function request_complete(RequestCompleteRequest $request){
        return $request->persist();
    }
    /**
     * @param ReviewRequest $request
     * @return JsonResponse
     */
    public function review(ReviewRequest $request){
        return $request->persist();
    }
    /**
     * @param ListRequest $request
     * @return JsonResponse
     */
    public function list(ListRequest $request){
        return $request->persist();
    }
    /**
     * @param DeleteMediaRequest $request
     * @return JsonResponse
     */
    public function delete_media(DeleteMediaRequest $request){
        return $request->persist();
    }
}
