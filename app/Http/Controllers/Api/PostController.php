<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Post\ArticleRequest;
use App\Http\Requests\Api\Post\QuestionRequest;
use App\Http\Requests\Api\Post\ShowArticleRequest;
use App\Http\Requests\Api\Post\ShowQuestionRequest;
use App\Http\Requests\Api\Post\ShowVideoRequest;
use App\Http\Requests\Api\Post\VideoRequest;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    /**
     * @param ArticleRequest $request
     * @return JsonResponse
     */
    public function articles(ArticleRequest $request){
        return $request->persist();
    }

    /**
     * @param QuestionRequest $request
     * @return JsonResponse
     */
    public function questions(QuestionRequest $request){
        return $request->persist();
    }

    /**
     * @param VideoRequest $request
     * @return mixed
     */
    public function videos(VideoRequest $request){
        return $request->persist();
    }

    /**
     * @param ShowArticleRequest $request
     * @return JsonResponse
     */
    public function show_article(ShowArticleRequest $request){
        return $request->persist();
    }

    /**
     * @param ShowQuestionRequest $request
     * @return JsonResponse
     */
    public function show_question(ShowQuestionRequest $request){
        return $request->persist();
    }

    /**
     * @param ShowVideoRequest $request
     * @return mixed
     */
    public function show_video(ShowVideoRequest $request){
        return $request->persist();
    }
}
