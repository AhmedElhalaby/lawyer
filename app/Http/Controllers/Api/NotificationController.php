<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Notification\ReadAllForm;
use App\Http\Requests\Api\Notification\ReadForm;
use App\Http\Requests\Api\Notification\SearchForm;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class NotificationController extends Controller
{
    use ResponseTrait;

    /**
     * @param SearchForm $form
     * @return JsonResponse
     */
    public function index(SearchForm $form){
        return $form->persist();
    }

    /**
     * @param ReadForm $form
     * @return JsonResponse
     */
    public function read(ReadForm $form){
        return $form->persist();
    }

    /**
     * @param ReadAllForm $form
     * @return JsonResponse
     */
    public function read_all(ReadAllForm $form){
        return $form->persist();
    }

}
