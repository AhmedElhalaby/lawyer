<?php

namespace App\Http\Controllers\Admin\AppData;

use App\Http\Controllers\Admin\Controller;
use App\Models\Time;
use App\Traits\AhmedPanelTrait;

class TimeController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/app_data/times');
        $this->setEntity(new Time());
        $this->setTable('times');
        $this->setLang('Time');
        $this->setExport(false);
        $this->setColumns([
            'time'=> [
                'name'=>'time',
                'type'=>'time',
                'is_searchable'=>true,
                'order'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->setFields([
            'time'=> [
                'name'=>'time',
                'type'=>'time',
                'is_required'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_required'=>true
            ],
        ]);
        $this->SetLinks([
            'edit',
        ]);
    }

}
