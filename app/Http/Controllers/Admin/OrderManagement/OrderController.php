<?php

namespace App\Http\Controllers\Admin\OrderManagement;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Controllers\Admin\Controller;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Models\User;
use App\Traits\AhmedPanelTrait;

class OrderController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/order_managements/orders');
        $this->setEntity(new Order());
        $this->setCreate(false);
        $this->setTable('orders');
        $this->setLang('Order');
        $this->setColumns([
            'id'=> [
                'name'=>'id',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'user_id'=> [
                'name'=>'user_id',
                'type'=>'custom_relation',
                'relation'=>[
                    'data'=> User::where('type',Constant::USER_TYPE['Customer'])->get(),
                    'custom'=>function($Object){
                        return $Object->first_name .' '.$Object->last_name;
                    },
                    'entity'=>'user'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'lawyer_id'=> [
                'name'=>'lawyer_id',
                'type'=>'custom_relation',
                'relation'=>[
                    'data'=> Lawyer::all(),
                    'custom'=>function($Object){
                        return ($Object)?$Object->user->first_name .' '. $Object->user->last_name:' - ';
                    },
                    'entity'=>'lawyer'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'category_id'=> [
                'name'=>'category_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> Category::all(),
                    'name'=>(session('my_locale') == 'ar')?'name_ar':'name',
                    'entity'=>'category'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            'description'=> [
                'name'=>'description',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'price'=> [
                'name'=>'price',
                'type'=>'custom_relation',
                'relation'=>[
                    'data'=> OrderOffer::all(),
                    'custom'=>function($Object){
                        return ($Object)?$Object->price:'-';
                    },
                    'entity'=>'order_offer'
                ],
                'is_searchable'=>false,
                'order'=>false
            ],
            'status'=> [
                'name'=>'status',
                'type'=>'select',
                'data'=>[
                    Constant::ORDER_STATUS['WaitingOffers'] =>__('crud.Order.Statuses.'.Constant::ORDER_STATUS['WaitingOffers'],[],session('my_locale')),
                    Constant::ORDER_STATUS['SelectingOffer'] =>__('crud.Order.Statuses.'.Constant::ORDER_STATUS['SelectingOffer'],[],session('my_locale')),
                    Constant::ORDER_STATUS['Processing'] =>__('crud.Order.Statuses.'.Constant::ORDER_STATUS['Processing'],[],session('my_locale')),
                    Constant::ORDER_STATUS['Cancel'] =>__('crud.Order.Statuses.'.Constant::ORDER_STATUS['Cancel'],[],session('my_locale')),
                    Constant::ORDER_STATUS['Completed'] =>__('crud.Order.Statuses.'.Constant::ORDER_STATUS['Completed'],[],session('my_locale')),
                    Constant::ORDER_STATUS['WaitingClientEndApprove'] =>__('crud.Order.Statuses.'.Constant::ORDER_STATUS['WaitingClientEndApprove'],[],session('my_locale')),
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([
        ]);
    }
}
