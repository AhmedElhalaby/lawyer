<?php

namespace App\Http\Controllers\Admin\AppPost;

use App\Helpers\Constant;
use App\Http\Controllers\Admin\Controller;
use App\Models\Category;
use App\Models\Post;
use App\Traits\AhmedPanelTrait;

class VideoController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/app_posts/videos');
        $this->setEntity(new Post());
        $this->setTable('posts');
        $this->setLang('Video');
        $this->setFilters([
            'type'=>[
                'name'=>'type',
                'value'=>Constant::POST_TYPE['Video']
            ]
        ]);
        $this->setColumns([
            'category_id'=> [
                'name'=>'category_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> Category::where('is_active',true)->get(),
                    'name'=>'name',
                    'entity'=>'category'
                ],
                'is_searchable'=>true,
                'order'=>true
            ],
            (app()->getLocale() == 'ar')?'title_ar':'title'=> [
                'name'=>(app()->getLocale() == 'ar')?'title_ar':'title',
                'type'=>'text-custom',
                'custom'=>function ($object){
                    return (app()->getLocale() == 'ar')?$object->getTitleAr():$object->getTitle();
                },
                'is_searchable'=>true,
                'order'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->setFields([
            'category_id'=> [
                'name'=>'category_id',
                'type'=>'relation',
                'relation'=>[
                    'data'=> Category::where('is_active',true)->get(),
                    'name'=>'name',
                    'entity'=>'category'
                ],
                'is_required'=>true
            ],
            'title'=> [
                'name'=>'title',
                'type'=>'text',
                'is_required'=>true
            ],
            'title_ar'=> [
                'name'=>'title_ar',
                'type'=>'text',
                'is_required'=>true
            ],
            'description'=> [
                'name'=>'description',
                'type'=>'textarea',
                'is_required'=>true
            ],
            'description_ar'=> [
                'name'=>'description_ar',
                'type'=>'textarea',
                'is_required'=>true
            ],
            'file'=> [
                'name'=>'file',
                'type'=>'text',
                'is_required'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_required'=>true
            ],
        ]);
        $this->SetLinks([
            'edit',
            'delete',
        ]);
    }

}
