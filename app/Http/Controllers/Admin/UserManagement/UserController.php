<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Controllers\Admin\Controller;
use App\Models\Transaction;
use App\Models\User;
use App\Traits\AhmedPanelTrait;

class UserController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/user_managements/users');
        $this->setEntity(new User());
        $this->setFilters([
            'type'=>[
                'name'=>'type',
                'value'=>Constant::USER_TYPE['Customer']
            ]
        ]);
        $this->setViewShow('Admin.UserManagement.User.show');
        $this->setCreate(false);
        $this->setTable('users');
        $this->setLang('User');
        $this->setColumns([
            'name'=> [
                'name'=>'name',
                'type'=>'text-custom',
                'custom'=>function ($Object){
                        return $Object->getFirstName().' '.$Object->getLastName();
                },
                'is_searchable'=>true,
                'order'=>true
            ],
            'mobile'=> [
                'name'=>'mobile',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'email'=> [
                'name'=>'email',
                'type'=>'email',
                'is_searchable'=>true,
                'order'=>true
            ],
            'balance'=> [
                'name'=>'balance',
                'type'=>'text-custom',
                'custom'=>function ($Object){
                    return Functions::UserBalance($Object->getId()) .'$';
                },
                'is_searchable'=>true,
                'order'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_searchable'=>true,
                'order'=>true
            ],
            'created_at'=> [
                'name'=>'created_at',
                'type'=>'datetime',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([
            'active',
            'show',
            'change_password',
        ]);
    }
    public function show($id)
    {
        $Object =$this->getEntity()->find($id);
        $Transactions = Transaction::where('user_id',$Object->getId())->paginate(6);
        if(!$Object)
            return $this->wrongData();
        return view($this->getViewShow(),compact('Object','Transactions'))->with($this->getParams());
    }
}
