<?php

namespace App\Http\Controllers\Admin\UserManagement;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Controllers\Admin\Controller;
use App\Models\LawyerCategory;
use App\Models\Transaction;
use App\Models\User;
use App\Traits\AhmedPanelTrait;

class LawyerController extends Controller
{
    use AhmedPanelTrait;

    public function setup()
    {
        $this->setRedirect('admin/user_managements/lawyers');
        $this->setEntity(new User());
        $this->setFilters([
            'type'=>[
                'name'=>'type',
                'value'=>Constant::USER_TYPE['Lawyer']
            ]
        ]);
        $this->setViewShow('Admin.UserManagement.Lawyer.show');
        $this->setCreate(false);
        $this->setTable('users');
        $this->setLang('Lawyer');
        $this->setColumns([
            'name'=> [
                'name'=>'name',
                'type'=>'text-custom',
                'custom'=>function ($Object){
                        return $Object->getFirstName().' '.$Object->getLastName();
                },
                'is_searchable'=>true,
                'order'=>true
            ],
            'mobile'=> [
                'name'=>'mobile',
                'type'=>'text',
                'is_searchable'=>true,
                'order'=>true
            ],
            'email'=> [
                'name'=>'email',
                'type'=>'email',
                'is_searchable'=>true,
                'order'=>true
            ],
            'is_active'=> [
                'name'=>'is_active',
                'type'=>'active',
                'is_searchable'=>true,
                'order'=>true
            ],
            'created_at'=> [
                'name'=>'created_at',
                'type'=>'datetime',
                'is_searchable'=>true,
                'order'=>true
            ],
        ]);
        $this->SetLinks([
            'active',
            'show',
            'change_password',
        ]);
    }
    public function show($id)
    {
        $Object =$this->getEntity()->find($id);
        if(!$Object->lawyer){
            return $this->wrongData();
        }
        $Transactions = Transaction::where('user_id',$Object->getId())->paginate(10);
        $LawyerCategories = LawyerCategory::where('lawyer_id',$Object->lawyer->id)->get();
        if(!$Object)
            return $this->wrongData();
        return view($this->getViewShow(),compact('Object','LawyerCategories','Transactions'))->with($this->getParams());
    }
}
