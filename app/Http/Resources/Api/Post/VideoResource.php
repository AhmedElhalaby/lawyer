<?php

namespace App\Http\Resources\Api\Post;

use App\Http\Resources\Api\General\CategoryResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $Objects = array();
        $Objects['id'] = $this->getId();
        $Objects['title'] = (app()->getLocale() == 'ar')?$this->getTitleAr():$this->getTitle();
        $Objects['description'] = (app()->getLocale() == 'ar')?$this->getDescriptionAr():$this->getDescription();
        $Objects['url'] =$this->getFile();
        $Objects['Category'] = new CategoryResource($this->category);
        $Objects['created_at'] = Carbon::parse($this->created_at)->format('Y-m-d');
        $Objects['Author'] = $this->author? [
            'name'=>$this->author->getName(),
            'image'=>asset($this->author->getImage())
        ]:null;
        return $Objects;
    }
}
