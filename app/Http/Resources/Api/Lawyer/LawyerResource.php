<?php

namespace App\Http\Resources\Api\Lawyer;

use App\Models\Order;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LawyerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['name'] = $this->user->getFirstName(). ' '. $this->user->getLastName();
        $Object['image'] = ($this->user->getImage())?asset($this->user->getImage()):null;
        $Object['union_membership'] = $this->getUnionMembership()?asset($this->getUnionMembership()):null;
        $Object['bio'] = $this->getBio();
        $Object['school_certificate'] = $this->getSchoolCertificate()?asset($this->getSchoolCertificate()):null;
        $Object['educational_level'] = $this->getEducationalLevel();
        $Object['work_address'] = $this->getWorkAddress();
        $Object['practice_certificate'] = $this->getPracticeCertificate()?asset($this->getPracticeCertificate()):null;
        $Object['contact_email'] = $this->getContactEmail();
        $Object['contact_mobile'] = $this->getContactMobile();
        $Object['Categories'] = LawyerCategoryResource::collection($this->lawyer_categories);
        $Object['order_count'] = Order::where('lawyer_id',$this->getId())->count();
        $Orders = Order::where('lawyer_id',$this->getId())->pluck('id');
        $Object['rate'] = Review::whereIn('order_id',$Orders)->avg('rate');


        return $Object;
    }

}
