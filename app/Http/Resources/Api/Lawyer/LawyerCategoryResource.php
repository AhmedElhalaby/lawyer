<?php

namespace App\Http\Resources\Api\Lawyer;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class LawyerCategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $Objects = array();
        $Objects['id'] = $this->category->getId();
        $Objects['name'] = (app()->getLocale() == 'ar')?$this->category->getNameAr():$this->category->getName();
        $Objects['image'] = $this->category->getImage();
        return $Objects;
    }
}
