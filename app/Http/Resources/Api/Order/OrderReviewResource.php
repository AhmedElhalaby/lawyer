<?php

namespace App\Http\Resources\Api\Order;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['rate'] = $this->getRate();
        $Object['review'] = $this->getReview();
        return $Object;
    }

}
