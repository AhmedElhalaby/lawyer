<?php

namespace App\Http\Resources\Api\Order;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getFirstName() .' '. $this->getLastName();
        $Object['mobile'] = $this->getMobile();
        $Object['email'] = $this->getEmail();
        $Object['gender'] = $this->getGender();
        $Object['image'] =  ($this->getImage())?asset($this->getImage()):null;
        return $Object;
    }

}
