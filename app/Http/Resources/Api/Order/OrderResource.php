<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\General\CategoryResource;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['Lawyer'] = $this->lawyer? new LawyerResource($this->lawyer):null;
        $Object['Category'] = new CategoryResource($this->category);
        $Object['User'] = new UserResource($this->user);
        $Object['description'] = $this->description;
        $Object['OrderDates'] = OrderDateResource::collection($this->order_dates);
        $Object['OrderTimes'] = OrderTimeResource::collection($this->order_times);
        $Object['OrderMedia'] = OrderMediaResource::collection($this->order_media);
        $Object['OrderReview'] = ($this->order_review)?new OrderReviewResource($this->order_review):null;
        $Object['order_date'] = ($this->order_offer)?$this->order_offer->order_date->date:null;
        $Object['order_time'] = ($this->order_offer)?$this->order_offer->order_time->time->time:null;
        $Object['status'] = $this->status;
        return $Object;
    }

}
