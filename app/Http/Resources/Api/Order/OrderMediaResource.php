<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\General\CategoryResource;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderMediaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['file'] = asset($this->getFile());
        $Object['created_at'] = Carbon::parse($this->created_at)->format('Y-m-d');
        return $Object;
    }

}
