<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\General\CategoryResource;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['user_name'] = $this->user? $this->user->first_name .' '.$this->user->last_name:null;
        $Object['lawyer_name'] = $this->lawyer? $this->lawyer->user->first_name .' '.$this->lawyer->user->last_name:null;
        $Object['description'] = $this->description;
        return $Object;
    }

}
