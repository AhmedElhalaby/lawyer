<?php

namespace App\Http\Resources\Api\Order;

use App\Http\Resources\Api\General\CategoryResource;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['Lawyer'] = new LawyerResource($this->lawyer);
        $Object['order_time'] = $this->order_time->time->time;
        $Object['order_date'] = $this->order_date->date;
        $Object['price'] = $this->price;
        $Object['note'] = $this->note;
        $Object['created_at'] = Carbon::parse($this->created_at)->format('Y-m-d');
        return $Object;
    }

}
