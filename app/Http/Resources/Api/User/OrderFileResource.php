<?php

namespace App\Http\Resources\Api\User;

use App\Http\Resources\Api\Order\OrderMediaResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderFileResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['name'] = $this->getDescription();
        $Object['OrderMedia'] = OrderMediaResource::collection($this->order_media);
        return $Object;
    }

}
