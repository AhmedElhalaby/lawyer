<?php

namespace App\Http\Resources\Api\User;

use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    protected $token;

    /**
     * ExportResource constructor.
     * @param $resource
     * @param array $token
     */
    public function __construct($resource, $token =null)
    {
        $this->token = $token;
        parent::__construct($resource);
    }
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $Object['id'] = $this->getId();
        $Object['first_name'] = $this->getFirstName();
        $Object['last_name'] = $this->getLastName();
        $Object['mobile'] = $this->getMobile();
        $Object['email'] = $this->getEmail();
        $Object['dob'] = $this->getDob();
        $Object['gender'] = $this->getGender().'';
        $Object['mobile_verified_at'] = $this->getMobileVerifiedAt();
        $Object['email_verified_at'] = $this->getEmailVerifiedAt();
        $Object['type'] = $this->getType();
        $Object['image'] = ($this->getImage())?asset($this->getImage()):null;
        $Object['app_locale'] = $this->getAppLocale();
        $Object['notification_count'] = Notification::where('user_id',$this->id)->where('read_at',null)->count();
        $Object['access_token'] = $this->token;
        $Object['token_type'] = 'Bearer';
        return $Object;
    }

}
