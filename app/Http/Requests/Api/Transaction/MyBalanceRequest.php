<?php

namespace App\Http\Requests\Api\Transaction;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Transaction;
use App\Traits\ResponseTrait;

class MyBalanceRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $AvailableBalance = Functions::UserBalance(auth()->user()->id);
        $HoldBalance = Transaction::where('user_id',auth()->user()->id)->where('type',Constant::TRANSACTION_TYPES['Hold'])->sum('value');
        $Balance = [
            'AvailableBalance'=>$AvailableBalance,
            'HeldBalance'=>$HoldBalance,
            'TotalBalance'=>$AvailableBalance+$HoldBalance
        ];
        return $this->successJsonResponse([],$Balance,'Balance');
    }
}
