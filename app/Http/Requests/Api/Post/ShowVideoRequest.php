<?php

namespace App\Http\Requests\Api\Post;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Post\ArticleResource;
use App\Http\Resources\Api\Post\QuestionResource;
use App\Http\Resources\Api\Post\VideoResource;
use App\Models\InterestingCategory;
use App\Models\Post;
use App\Traits\ResponseTrait;

class ShowVideoRequest extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'video_id'=>'required|exists:posts,id'
        ];
    }

    public function persist()
    {
        $Object = (new Post())->find($this->video_id);
        return $this->successJsonResponse([],new VideoResource($Object),'Video');
    }
}
