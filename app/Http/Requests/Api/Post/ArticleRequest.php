<?php

namespace App\Http\Requests\Api\Post;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Post\ArticleResource;
use App\Models\InterestingCategory;
use App\Models\Post;
use App\Traits\ResponseTrait;

class ArticleRequest extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $Objects = new Post();
        $Objects = $Objects->where('is_active',true);
        if($this->filled('q')){
            if (app()->getLocale() == 'ar'){
                $Objects = $Objects->where(function ($query) {
                    return $query->where('title_ar', 'LIKE',"%{$this->q}%")
                        ->orWhere('summery_ar', 'LIKE',"%{$this->q}%")
                        ->orWhere('description_ar', 'LIKE',"%{$this->q}%");
                });
            }else{
                $Objects = $Objects->where(function ($query) {
                    return $query->where('title', 'LIKE',"%{$this->q}%")
                        ->orWhere('summery', 'LIKE',"%{$this->q}%")
                        ->orWhere('description', 'LIKE',"%{$this->q}%");
                });
            }
        }
        $Objects = $Objects->where('type',Constant::POST_TYPE['Post']);
        if($this->filled('category_id')){
            $Objects = $Objects->where('category_id',$this->category_id);
        }else{
            if(auth('api')->check()){
                $Categories = InterestingCategory::where('user_id',auth()->user()->id)->pluck('category_id');
                if (count($Categories) != 0){
                    $Objects = $Objects->whereIn('category_id',$Categories);
                    if($Objects->whereIn('category_id',$Categories)->count() < 3){
                        $Objects = new Post();
                        $Objects = $Objects->where('type',Constant::POST_TYPE['Post']);
                    }

                }
            }
        }

        $Objects = $Objects->paginate($this->filled('per_page')?$this->per_page:10);
        return $this->successJsonResponse([],ArticleResource::collection($Objects->items()),'Articles',$Objects);
    }
}
