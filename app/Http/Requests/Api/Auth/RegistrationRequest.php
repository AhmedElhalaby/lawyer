<?php

namespace App\Http\Requests\Api\Auth;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\User\UserResource;
use App\Models\Lawyer;
use App\Traits\ResponseTrait;
use App\Models\User;

/**
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed password
 * @property mixed mobile
 * @property mixed email
 * @property mixed dob
 * @property mixed type
 * @property mixed gender
 * @property mixed device_token
 * @property mixed device_type
 */
class RegistrationRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'password' => 'required|string|min:6',
            'mobile' => 'required|numeric|unique:users',
            'email' => 'required|email|unique:users',
            'type' => 'required|in:'.Constant::USER_TYPE_RULES,
            'dob' => 'sometimes|date',
            'gender' => 'required|numeric|in:'.Constant::USER_GENDER_RULES,
            'device_token' => 'string|required_with:device_type',
            'device_type' => 'string|required_with:device_token',
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $user = new User();
        $user->setFirstName($this->first_name);
        $user->setLastName($this->last_name);
        $user->setPassword($this->password);
        $user->setMobile($this->mobile);
        $user->setEmail($this->email);
        $user->setDob($this->dob);
        $user->setGender($this->gender);
        $user->setType($this->type);
        if ($this->filled('device_token') && $this->filled('device_type')) {
            $user->setDeviceToken($this->device_token);
            $user->setDeviceType($this->device_type);
        }
        $user->save();
        if ($this->type == Constant::USER_TYPE['Lawyer']){
            $Lawyer = new Lawyer();
            $Lawyer->setUserId($user->getId());
            $Lawyer->setContactEmail($this->email);
            $Lawyer->setContactMobile($this->mobile);
            $Lawyer->save();
            $user->setIsActive(false);
            $user->save();
        }
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        try{
            Functions::SendVerification($user);
        }catch(\Exception $e){
        }

        return $this->successJsonResponse( [__('messages.saved_successfully')],new UserResource($user,$tokenResult->accessToken),'User');

    }

}
