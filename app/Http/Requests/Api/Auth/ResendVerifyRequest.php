<?php

namespace App\Http\Requests\Api\Auth;

use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Traits\ResponseTrait;

class ResendVerifyRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function attributes()
    {
        return [];
    }
    public function persist()
    {
        $logged = auth('api')->user();
        Functions::SendVerification($logged);
        return $this->successJsonResponse( [__('auth.verification_code_sent')]);
    }
}
