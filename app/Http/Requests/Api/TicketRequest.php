<?php

namespace App\Http\Requests\Api;

use App\Http\Resources\Api\General\TicketResource;
use App\Models\Ticket;
use App\Traits\ResponseTrait;

class TicketRequest extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Objects =new  Ticket();
        $Objects = $Objects->where('user_id',$logged->getId());
        $Objects = $Objects->paginate($this->filled('per_page')?$this->per_page:10);
        return $this->successJsonResponse([],TicketResource::collection($Objects->items()),'Tickets',$Objects);
    }
}
