<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderOfferResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Traits\ResponseTrait;

class GetOfferRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Order = (new Order)->find($this->order_id);
        if($Order->getUserId() != $logged->getId()){
            return $this->failJsonResponse([__('messages.you_are_not_allowed')],403);
        }
        $Objects = new OrderOffer();
        $Objects = $Objects->with(['lawyer','order_time','order_date']);
        $Objects = $Objects->where('order_id',$Order->getId());
        $Objects = $Objects->paginate($this->filled('per_page')?$this->per_page:10);
        return $this->successJsonResponse([],OrderOfferResource::collection($Objects->items()),'OrderOffers',$Objects);
    }
}
