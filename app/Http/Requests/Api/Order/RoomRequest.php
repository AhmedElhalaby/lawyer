<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Order;
use App\Traits\ResponseTrait;

class RoomRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Objects = new Order();
        if($logged->getType() == Constant::USER_TYPE['Customer']){
            $Objects = $Objects->where('user_id',$logged->getId());
        }
        if ($logged->getType() == Constant::USER_TYPE['Lawyer'] ){
            $Objects = $Objects->where('lawyer_id',$logged->lawyer->getId());
        }
        $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUS['Processing'],Constant::ORDER_STATUS['WaitingClientEndApprove'],Constant::ORDER_STATUS['Completed']]);
        $Objects = $Objects->paginate($this->filled('per_page')?$this->per_page:10);
        return $this->successJsonResponse([],OrderResource::collection($Objects->items()),'Orders',$Objects);
    }
}
