<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Lawyer;
use App\Models\LawyerCategory;
use App\Models\Order;
use App\Models\OrderDate;
use App\Models\OrderMedia;
use App\Models\OrderTime;
use App\Models\User;
use App\Traits\ResponseTrait;

class StoreRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->getType() == Constant::USER_TYPE['Customer'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'required|exists:categories,id',
            'lawyer_id'=>'sometimes|exists:lawyers,id',
            'order_dates'=>'required|array',
            'order_dates.*'=>'required|date',
            'order_times'=>'required|array',
            'order_times.*'=>'required|exists:times,id',
            'attachments'=>'sometimes|array',
            'attachments.*'=>'sometimes|mimes:jpeg,jpg,png'
        ];
    }

    public function persist()
    {
        try {
            $logged = auth()->user();
            if (!$logged->isIsActive()) {
                return $this->failJsonResponse([__('admin.messages.your_account_is_in_active')]);
            }
            $Object = new Order();
            $Object->setUserId($logged->getId());
            $Object->setLawyerId($this->lawyer_id ?? null);
            $Object->setCategoryId($this->category_id);
            $Object->setDescription($this->description ?? null);
            $Object->save();
            foreach ($this->order_dates as $order_date){
                $OrderDate = new OrderDate();
                $OrderDate->setOrderId($Object->getId());
                $OrderDate->setDate($order_date);
                $OrderDate->save();
            }
            foreach ($this->order_times as $order_time){
                $OrderTime = new OrderTime();
                $OrderTime->setOrderId($Object->getId());
                $OrderTime->setTimeId($order_time);
                $OrderTime->save();
            }
            if ($this->hasFile('attachments')){
                foreach ($this->file('attachments') as $attachment){
                    $OrderMedia = new OrderMedia();
                    $OrderMedia->setOrderId($Object->getId());
                    $OrderMedia->setFile($attachment);
                    $OrderMedia->save();
                }
            }

            if($Object->getLawyerId() == null){
                $LawyersIds = LawyerCategory::where('category_id',$Object->getCategoryId())->pluck('lawyer_id');
                $UsersIds = Lawyer::whereIn('id',$LawyersIds)->pluck('user_id');
                $Users = User::whereIn('id',$UsersIds)->get();
                Functions::SendNotifications($Users,'طلب جديد','يوجد طلب استشارة جديد بامكانك اضافة عرض له',$Object->getId(),Constant::NOTIFICATION_TYPE['Order'],false);
            }else{
                Functions::SendNotification($Object->lawyer->user,'New','There is a new order you can place an offer for','طلب جديد','يوجد طلب استشارة جديد بامكانك اضافة عرض له',$Object->getId(),Constant::NOTIFICATION_TYPE['Order'],false);
            }
            return $this->successJsonResponse([__('messages.saved_successfully')],new OrderResource($Object),'Order');
        }
        catch (\Exception $e){
            return $this->failJsonResponse([__('Error : '.$e->getMessage())]);
        }
    }
}
