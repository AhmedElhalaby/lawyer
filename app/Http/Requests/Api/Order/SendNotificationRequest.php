<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\Lawyer;
use App\Models\Order;
use App\Traits\ResponseTrait;

class SendNotificationRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'lawyer_id'=>'nullable|exists:lawyers,id',
            'message'=>'required|string'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = (new Order)->find($this->order_id);
        if($logged->getType() == Constant::USER_TYPE['Customer']){
            if ($this->filled('lawyer_id')) {
                $User = ((new Lawyer)->find($this->lawyer_id))->user;
            }else{
                $User = $Object->lawyer->user;
            }
        }
        if ($logged->getType() == Constant::USER_TYPE['Lawyer']){
            $User = $Object->user;
        }
        Functions::SendNotification($User,'You have new message',$this->message,'لديك رسالة جديدة',$this->message,$Object->getId(),Constant::NOTIFICATION_TYPE['NewMessage']);
        return $this->successJsonResponse([]);
    }
}
