<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\Lawyer;
use App\Models\LawyerCategory;
use App\Models\Order;
use App\Models\OrderDate;
use App\Models\OrderOffer;
use App\Models\OrderTime;
use App\Models\Transaction;
use App\Models\User;
use App\Traits\ResponseTrait;

/**
 * @property integer order_id
 * @property integer order_offer_id
 * @property integer status
 */
class UpdateRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'status'=>'required|in:'.Constant::ORDER_STATUS_RULES,
            'order_offer_id'=>'required_if:status,'.Constant::ORDER_STATUS['SelectingOffer'].'|exists:order_offers,id',
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Order = (new Order)->find($this->order_id);
        Functions::CheckOrderSequence($Order,$this->status);
        switch ($this->status){
            case Constant::ORDER_STATUS['SelectingOffer']:{
                $OrderOffer = (new OrderOffer)->find($this->order_offer_id);
                $Order->setLawyerId($OrderOffer->getLawyerId());
                $Order->setOrderOfferId($OrderOffer->getId());
                $Order->setStatus(Constant::ORDER_STATUS['SelectingOffer']);
                $Order->setOrderTime($OrderOffer->order_time->time->time);
                $Order->setOrderDate($OrderOffer->order_date->date);
                $Order->save();
//                Functions::SendNotification($Order->lawyer->user,'Offer Selected','Your offer on the order is selected by client','تم اختيار العرض','لقد تم اختيار العرض الخاص بك من قبل العميل',$Order->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
                break;
            }
            case Constant::ORDER_STATUS['Processing']:{
                if (Functions::UserBalance($logged->id) >= $Order->order_offer->price ){
                    $Transaction = new Transaction();
                    $Transaction->setOrderId($Order->getId());
                    $Transaction->setUserId($logged->id);
                    $Transaction->setValue($Order->order_offer->price);
                    $Transaction->setType(Constant::TRANSACTION_TYPES['Withdraw']);
                    $Transaction->save();
                    $Order->setStatus(Constant::ORDER_STATUS['Processing']);
                    $Order->save();
                    Functions::SendNotification($Order->lawyer->user,'Order Confirmed','The order has been confirmed by the client','تأكيد الاستشارة','لقد تم تأكيد موعد الاستشارة من قبل العميل',$Order->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
                }else{
                    return $this->failJsonResponse([__('messages.dont_have_enough_credit')]);
                }
                break;
            }
            case Constant::ORDER_STATUS['Completed']:{
                $Order->setStatus(Constant::ORDER_STATUS['Completed']);
                $Order->save();
                $Transaction = new Transaction();
                $Transaction->setOrderId($Order->getId());
                $Transaction->setUserId($Order->lawyer->user_id);
                $Transaction->setValue(($Order->order_offer->price*75)/100);
                $Transaction->setType(Constant::TRANSACTION_TYPES['Hold']);
                $Transaction->setStatus(Constant::TRANSACTION_STATUS['Paid']);
                $Transaction->save();
                Functions::SendNotification($Order->lawyer->user,'Order Completed','The order has been completed ','إنهاء الاستشارة','لقد تم انهاء الاستشارة',$Order->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
                break;
            }
            case Constant::ORDER_STATUS['Cancel']:{
                $Order->setStatus(Constant::ORDER_STATUS['Cancel']);
                $Order->save();
                break;
            }
        }
        return $this->successJsonResponse([__('messages.updated_successful')],new ShowOrderResource($Order),'Order');
    }
}
