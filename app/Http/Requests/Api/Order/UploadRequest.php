<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderMediaResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\Order;
use App\Models\OrderMedia;
use App\Traits\ResponseTrait;

/**
 * @property integer order_id
 * @property mixed attachment
 */
class UploadRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'attachment'=>'required|mimes:jpeg,jpg,png,pdf,doc,docx,xlsx,xls'
        ];
    }

    public function persist()
    {
        try {
            $logged = auth()->user();
            $Order = (new Order)->find($this->order_id);
            $OrderMedia = new OrderMedia();
            $OrderMedia->setOrderId($Order->getId());
            $OrderMedia->setFile($this->file('attachment'));
            $OrderMedia->save();
            return $this->successJsonResponse([__('messages.updated_successful')],new OrderMediaResource($OrderMedia),'OrderMedia');
        }
        catch (\Exception $e){
            return $this->failJsonResponse([__('Error : '.$e->getMessage())]);
        }
    }
}
