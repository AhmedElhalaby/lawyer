<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderMediaResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\Order;
use App\Models\OrderMedia;
use App\Models\Review;
use App\Traits\ResponseTrait;

/**
 * @property integer order_id
 * @property mixed attachment
 */
class ReviewRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'rate'=>'required|numeric',
        ];
    }

    public function persist()
    {
        try {
            $logged = auth()->user();
            $Order = (new Order)->find($this->order_id);
            if ($Order->getStatus() != Constant::ORDER_STATUS['Completed']){
                return $this->failJsonResponse([__('messages.wrong_sequence')]);
            }
            if (Review::where('order_id',$Order->getId())->first()){
                return $this->failJsonResponse([__('messages.you_cannot_do_it_again')]);
            }
            $Review = new Review();
            $Review->setOrderId($Order->getId());
            $Review->setUserId($logged->getId());
            $Review->setLawyerId($Order->getLawyerId());
            $Review->setRate($this->rate);
            $Review->setReview($this->review);
            $Review->save();
            return $this->successJsonResponse([__('messages.updated_successful')],new OrderResource($Order),'Order');
        }
        catch (\Exception $e){
            return $this->failJsonResponse([__('Error : '.$e->getMessage())]);
        }
    }
}
