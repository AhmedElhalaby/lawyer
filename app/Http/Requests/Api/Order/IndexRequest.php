<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Order;
use App\Traits\ResponseTrait;

class IndexRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Objects = new Order();
        if($logged->getType() == Constant::USER_TYPE['Customer']){
            $Objects = $Objects->where('user_id',$logged->getId());
            if ($this->filled('status')){
                if($this->status == Constant::ORDER_STATUS['WaitingOffers'] || $this->status == Constant::ORDER_STATUS['SelectingOffer'] ){
                    $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUS['WaitingOffers'],Constant::ORDER_STATUS['SelectingOffer']]);
                }elseif ($this->status == Constant::ORDER_STATUS['Processing'] || $this->status == Constant::ORDER_STATUS['WaitingClientEndApprove'] ){
                    $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUS['Processing'],Constant::ORDER_STATUS['WaitingClientEndApprove']]);
                }else{
                    $Objects = $Objects->where('status',$this->status);
                }
            }
        }
        if ($logged->getType() == Constant::USER_TYPE['Lawyer'] ){
            if ($this->filled('mine') &&$this->mine == true){
                $Objects = $Objects->where('lawyer_id',$logged->lawyer->getId());
                if ($this->filled('status')){
                    if($this->status == Constant::ORDER_STATUS['WaitingOffers'] || $this->status == Constant::ORDER_STATUS['SelectingOffer'] ){
                        $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUS['WaitingOffers'],Constant::ORDER_STATUS['SelectingOffer']]);
                    }elseif ($this->status == Constant::ORDER_STATUS['Processing'] || $this->status == Constant::ORDER_STATUS['WaitingClientEndApprove'] ){
                        $Objects = $Objects->whereIn('status',[Constant::ORDER_STATUS['Processing'],Constant::ORDER_STATUS['WaitingClientEndApprove']]);
                    }else{
                        $Objects = $Objects->where('status',$this->status);
                    }
                }
            }
            else{
                $Objects = $Objects->where('lawyer_id',null)->where('status',Constant::ORDER_STATUS['WaitingOffers']);
            }
        }
        $Objects = $Objects->paginate($this->filled('per_page')?$this->per_page:10);
        return $this->successJsonResponse([],OrderResource::collection($Objects->items()),'Orders',$Objects);
    }
}
