<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\CompleteRequest;
use App\Models\Order;
use App\Traits\ResponseTrait;

class RequestCompleteRequest extends ApiRequest
{
    use ResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->getType() == Constant::USER_TYPE['Lawyer'] ;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = (new Order)->find($this->order_id);

        if($Object->getLawyerId() != $logged->lawyer->id){
            return $this->failJsonResponse([__('messages.dont_have_permission')]);
        }
        if(CompleteRequest::where('order_id',$Object->getId())->first()){
            return $this->failJsonResponse([__('messages.you_cannot_do_it_again')]);
        }
        $RequestComplete = new CompleteRequest();
        $RequestComplete->setOrderId($Object->getId());
        $RequestComplete->save();
        $Object->setStatus(Constant::ORDER_STATUS['WaitingClientEndApprove']);
        $Object->save();
        return $this->successJsonResponse([__('messages.saved_successfully')]);
    }
}
