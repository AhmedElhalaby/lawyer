<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\Order;
use App\Models\Review;
use App\Traits\ResponseTrait;

class ShowRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = (new Order)->find($this->order_id);
        if($logged->getType() == Constant::USER_TYPE['Customer']){
            if ($Object->getUserId() != $logged->getId()){
                return $this->failJsonResponse([__('messages.dont_have_permission')]);
            }
        }
        if ($Object->getLawyerId() != null && $logged->getType() == Constant::USER_TYPE['Lawyer']){
            if ($Object->getLawyerId() != $logged->lawyer->getId()){
                return $this->failJsonResponse([__('messages.dont_have_permission')]);
            }
        }
        return $this->successJsonResponse([],new ShowOrderResource($Object),'Order');
    }
}
