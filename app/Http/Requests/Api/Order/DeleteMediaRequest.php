<?php

namespace App\Http\Requests\Api\Order;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Order\OrderMediaResource;
use App\Http\Resources\Api\Order\ShowOrderResource;
use App\Models\Order;
use App\Models\OrderMedia;
use App\Traits\ResponseTrait;

/**
 * @property integer media_id
 * @property integer file_name
 */
class DeleteMediaRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'media_id'=>'required_without:file_name|exists:order_media,id',
            'file_name'=>'required_without:media_id|string'
        ];
    }

    public function persist()
    {
        try {
            if ($this->filled('media_id')) {
                $OrderMedia = (new OrderMedia())->find($this->media_id);
            }else{
                if ($this->filled('file_name')) {
                    $OrderMedia = (new OrderMedia())->where('file',$this->file_name)->first();
                }
            }
            if (isset($OrderMedia) && $OrderMedia) {
                $OrderMedia->delete();
            }else{
                return $this->failJsonResponse([__('messages.object_not_found')]);
            }
            return $this->successJsonResponse([__('messages.deleted_successful')]);
        }
        catch (\Exception $e){
            return $this->failJsonResponse([__('Error : '.$e->getMessage())]);
        }
    }
}
