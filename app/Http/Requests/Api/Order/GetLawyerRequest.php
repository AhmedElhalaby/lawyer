<?php

namespace App\Http\Requests\Api\Order;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Http\Resources\Api\Order\OrderResource;
use App\Models\Category;
use App\Models\Lawyer;
use App\Models\LawyerCategory;
use App\Models\Order;
use App\Models\User;
use App\Traits\ResponseTrait;

class GetLawyerRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'sometimes|exists:categories,id'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $NotActiveLawyers = User::where('is_active',false)->where('type',Constant::USER_TYPE['Lawyer'])->pluck('id');
        $Objects = new Lawyer();
        $Objects = $Objects->whereNotIn('user_id',$NotActiveLawyers);
        if($this->filled('category_id')){
            $LawyerIds = LawyerCategory::where('category_id',$this->category_id)->pluck('lawyer_id');
            $Objects = $Objects->whereIn('id',$LawyerIds);
        }
        $Objects = $Objects->get();
        return $this->successJsonResponse([],LawyerResource::collection($Objects),'Lawyers');
    }
}
