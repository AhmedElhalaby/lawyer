<?php

namespace App\Http\Requests\Api\Lawyer;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Models\LawyerCategory;
use App\Traits\ResponseTrait;

class UpdateLawyerForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->getType() == Constant::USER_TYPE['Lawyer'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lawyer_categories'=>'array',
            'lawyer_categories.*'=>'exists:categories,id'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = $logged->lawyer;
        if($this->hasFile('union_membership')){
            $Object->setUnionMembership($this->union_membership);
        }
        if($this->filled('bio')){
            $Object->setBio($this->bio);
        }
        if($this->hasFile('school_certificate')){
            $Object->setSchoolCertificate($this->school_certificate);
        }
        if($this->filled('educational_level')){
            $Object->setEducationalLevel($this->educational_level);
        }
        if($this->filled('work_address')){
            $Object->setWorkAddress($this->work_address);
        }
        if($this->hasFile('practice_certificate')){
            $Object->setPracticeCertificate($this->practice_certificate);
        }
        if($this->filled('contact_email')){
            $Object->setContactEmail($this->contact_email);
        }
        if($this->filled('contact_mobile')){
            $Object->setContactMobile($this->contact_mobile);
        }
        if($this->filled('lawyer_categories')){
            LawyerCategory::where('lawyer_id',$Object->getId())->delete();
            foreach ($this->lawyer_categories as $lawyer_categories){
                LawyerCategory::create(['lawyer_id'=>$Object->getId(),'category_id'=>$lawyer_categories]);
            }
        }
        $Object->save();
        $Object->refresh();
        return $this->successJsonResponse([__('messages.saved_successfully')],new LawyerResource($Object),'Lawyer');
    }
}
