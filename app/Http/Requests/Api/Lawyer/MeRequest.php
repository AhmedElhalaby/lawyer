<?php

namespace App\Http\Requests\Api\Lawyer;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Traits\ResponseTrait;

class MeRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->getType() == Constant::USER_TYPE['Lawyer'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Object = $logged->lawyer;
        return $this->successJsonResponse([],new LawyerResource($Object),'Lawyer');
    }
}
