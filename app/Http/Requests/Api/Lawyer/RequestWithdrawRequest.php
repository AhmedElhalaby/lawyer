<?php

namespace App\Http\Requests\Api\Lawyer;

use App\Helpers\Constant;
use App\Helpers\Functions;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Models\WithdrawRequest;
use App\Traits\ResponseTrait;

class RequestWithdrawRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user()->getType() == Constant::USER_TYPE['Lawyer'];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value'=>'required|numeric|max:500',
            'details'=>'required|string'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        if(Functions::UserBalance($logged->id) < $this->value){
            return $this->failJsonResponse([__('messages.dont_have_enough_credit')]);
        }
        $WithdrawRequest = new WithdrawRequest();
        $WithdrawRequest->setValue($this->value);
        $WithdrawRequest->setUserId($logged->id);
        $WithdrawRequest->setDetails($this->details);
        $WithdrawRequest->save();
        return $this->successJsonResponse([__('messages.created_successful')]);
    }
}
