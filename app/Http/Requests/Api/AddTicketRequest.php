<?php

namespace App\Http\Requests\Api;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\General\TicketResource;
use App\Models\Ticket;
use App\Traits\ResponseTrait;

class AddTicketRequest extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|string',
            'message'=>'required',
            'attachment'=>'sometimes|mimes:jpeg,jpg,png',
            'order_id'=>'sometimes|exists:orders,id'
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Ticket =new  Ticket();
        $Ticket->setUserId($logged->getId());
        $Ticket->setTitle($this->title);
        $Ticket->setMessage($this->message);
        $Ticket->setOrderId(@$this->order_id);
        if($this->hasFile('attachment')) {
            $Ticket->setAttachment($this->file('attachment'));
        }
        $Ticket->save();
        return $this->successJsonResponse([__('messages.saved_successfully')],new TicketResource($Ticket),'Ticket');
    }
}
