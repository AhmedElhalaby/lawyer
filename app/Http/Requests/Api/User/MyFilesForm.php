<?php

namespace App\Http\Requests\Api\User;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Http\Resources\Api\Order\OrderMediaResource;
use App\Http\Resources\Api\User\InterestingCategoryResource;
use App\Http\Resources\Api\User\OrderFileResource;
use App\Models\InterestingCategory;
use App\Models\Order;
use App\Models\OrderMedia;
use App\Models\User;
use App\Traits\ResponseTrait;

class MyFilesForm extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        $logged = auth()->user();
        $Orders = Order::where('user_id',$logged->getId())->paginate($this->filled('per_page')?$this->per_page:10);
        return $this->successJsonResponse([],OrderFileResource::collection($Orders->items()),'OrderFiles',$Orders);
    }
}
