<?php

namespace App\Http\Requests\Api\User;

use App\Helpers\Constant;
use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\Lawyer\LawyerResource;
use App\Http\Resources\Api\User\InterestingCategoryResource;
use App\Models\InterestingCategory;
use App\Models\User;
use App\Traits\ResponseTrait;

class MyInterestsForm extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }

    public function persist()
    {
        return $this->successJsonResponse([],InterestingCategoryResource::collection((new User)->with('interesting_categories')->find(auth()->user()->getId())->interesting_categories),'InterestingCategory');
    }
}
