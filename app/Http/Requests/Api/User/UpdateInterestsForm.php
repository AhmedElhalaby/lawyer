<?php

namespace App\Http\Requests\Api\User;

use App\Http\Requests\Api\ApiRequest;
use App\Http\Resources\Api\User\InterestingCategoryResource;
use App\Models\InterestingCategory;
use App\Models\User;
use App\Traits\ResponseTrait;

class UpdateInterestsForm extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'interesting_categories'=>'required|array',
            'interesting_categories.*'=>'required|exists:categories,id'
        ];
    }

    public function persist()
    {
        $Object = (new User)->with('interesting_categories')->find(auth()->user()->getId());
        InterestingCategory::where('user_id',$Object->getId())->delete();
        foreach ($this->interesting_categories as $interesting_categories){
            InterestingCategory::create(['user_id'=>$Object->getId(),'category_id'=>$interesting_categories]);
        }
        $Object->refresh();
        return $this->successJsonResponse([__('messages.saved_successfully')],InterestingCategoryResource::collection($Object->interesting_categories),'InterestingCategory');
    }
}
