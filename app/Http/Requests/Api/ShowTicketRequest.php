<?php

namespace App\Http\Requests\Api;

use App\Http\Resources\Api\General\TicketResource;
use App\Models\Ticket;
use App\Traits\ResponseTrait;

class ShowTicketRequest extends ApiRequest
{
    use ResponseTrait;


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ticket_id'=>'required|exists:tickets,id'
        ];
    }

    public function persist()
    {
        $Object = (new  Ticket())->find($this->ticket_id);
        return $this->successJsonResponse([],new TicketResource($Object),'Ticket');
    }
}
