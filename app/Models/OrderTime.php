<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer id
 * @property integer order_id
 * @property integer time_id
 * @method static find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class OrderTime extends Model
{
    protected $table = 'order_times';
    protected $fillable = ['order_id','time_id'];

    /**
     * @return BelongsTo
     */
    public function order(){
        return $this->belongsTo(Order::class);
    }

    /**
     * @return BelongsTo
     */
    public function time(){
        return $this->belongsTo(Time::class);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->order_id;
    }

    /**
     * @param int $order_id
     */
    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

    /**
     * @return int
     */
    public function getTimeId(): int
    {
        return $this->time_id;
    }

    /**
     * @param int $time_id
     */
    public function setTimeId(int $time_id): void
    {
        $this->time_id = $time_id;
    }

}
