<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property integer id
 * @property integer user_id
 * @property integer category_id
 * @property integer|null lawyer_id
 * @property integer|null order_offer_id
 * @property mixed description
 * @property mixed order_time
 * @property mixed order_date
 * @property integer status
 * @property boolean is_notified
 * @method Order find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = ['user_id','category_id','lawyer_id','order_offer_id','description','status'];
    protected static function boot() {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
    /**
     * @return BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function lawyer(){
        return $this->belongsTo(Lawyer::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function order_offer(){
        return $this->belongsTo(OrderOffer::class,'order_offer_id','id');
    }

    /**
     * @return HasMany
     */
    public function order_offers(){
        return $this->hasMany(OrderOffer::class,'order_id','id');
    }

    /**
     * @return HasMany
     */
    public function order_dates(){
        return $this->hasMany(OrderDate::class,'order_id','id');
    }

    /**
     * @return HasMany
     */
    public function order_media(){
        return $this->hasMany(OrderMedia::class,'order_id','id');
    }


    /**
     * @return HasMany
     */
    public function order_times(){
        return $this->hasMany(OrderTime::class,'order_id','id');
    }
    /**
     * @return HasOne|null
     */
    public function order_review(){
        return $this->hasOne(Review::class,'order_id','id');
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * @return int|null
     */
    public function getLawyerId(): ?int
    {
        return $this->lawyer_id;
    }

    /**
     * @param int|null $lawyer_id
     */
    public function setLawyerId(?int $lawyer_id): void
    {
        $this->lawyer_id = $lawyer_id;
    }

    /**
     * @return int|null
     */
    public function getOrderOfferId(): ?int
    {
        return $this->order_offer_id;
    }

    /**
     * @param int|null $order_offer_id
     */
    public function setOrderOfferId(?int $order_offer_id): void
    {
        $this->order_offer_id = $order_offer_id;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return bool
     */
    public function isIsNotified(): bool
    {
        return $this->is_notified;
    }

    /**
     * @param bool $is_notified
     */
    public function setIsNotified(bool $is_notified): void
    {
        $this->is_notified = $is_notified;
    }

    /**
     * @return mixed
     */
    public function getOrderTime()
    {
        return $this->order_time;
    }

    /**
     * @param mixed $order_time
     */
    public function setOrderTime($order_time): void
    {
        $this->order_time = $order_time;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * @param mixed $order_date
     */
    public function setOrderDate($order_date): void
    {
        $this->order_date = $order_date;
    }

}
