<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer id
 * @property integer order_id
 * @property integer lawyer_id
 * @property integer user_id
 * @property integer rate
 * @property string|null review
 * @method static find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Review extends Model
{
    protected $table = 'reviews';
    protected $fillable = ['order_id','lawyer_id','user_id','rate','review'];

    public function order(){
        return $this->belongsTo(Order::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function lawyer(){
        return $this->belongsTo(Lawyer::class);
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->order_id;
    }

    /**
     * @param int $order_id
     */
    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

    /**
     * @return int
     */
    public function getLawyerId(): int
    {
        return $this->lawyer_id;
    }

    /**
     * @param int $lawyer_id
     */
    public function setLawyerId(int $lawyer_id): void
    {
        $this->lawyer_id = $lawyer_id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int
     */
    public function getRate(): int
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate(int $rate): void
    {
        $this->rate = $rate;
    }

    /**
     * @return string|null
     */
    public function getReview(): ?string
    {
        return $this->review;
    }

    /**
     * @param string|null $review
     */
    public function setReview(?string $review): void
    {
        $this->review = $review;
    }
}
