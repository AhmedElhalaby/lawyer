<?php

namespace App\Models;

use App\Helpers\Functions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use phpDocumentor\Reflection\Types\Mixed_;

/**
 * @property integer id
 * @property integer user_id
 * @property mixed union_membership
 * @property string|null bio
 * @property mixed school_certificate
 * @property string|null educational_level
 * @property string|null work_address
 * @property mixed practice_certificate
 * @property string|null contact_email
 * @property string|null contact_mobile
 * @property integer status
 * @method Lawyer find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Lawyer extends Model
{
    protected $table = 'lawyers';
    protected $fillable = ['user_id','union_membership','bio','school_certificate','educational_level','work_address','practice_certificate','contact_email','contact_mobile','status'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function lawyer_categories(){
        return $this->hasMany(LawyerCategory::class,'lawyer_id','id')->with('category');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUnionMembership()
    {
        return $this->union_membership;
    }

    /**
     * @param mixed $union_membership
     */
    public function setUnionMembership($union_membership): void
    {
        $this->union_membership = Functions::StoreImage('union_membership','lawyer');
    }

    /**
     * @return string|null
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string|null $bio
     */
    public function setBio(?string $bio): void
    {
        $this->bio = $bio;
    }

    /**
     * @return mixed
     */
    public function getSchoolCertificate()
    {
        return $this->school_certificate;
    }

    /**
     * @param mixed $school_certificate
     */
    public function setSchoolCertificate($school_certificate): void
    {
        $this->school_certificate = Functions::StoreImage('school_certificate','lawyer');
    }

    /**
     * @return string|null
     */
    public function getEducationalLevel(): ?string
    {
        return $this->educational_level;
    }

    /**
     * @param string|null $educational_level
     */
    public function setEducationalLevel(?string $educational_level): void
    {
        $this->educational_level = $educational_level;
    }

    /**
     * @return string|null
     */
    public function getWorkAddress(): ?string
    {
        return $this->work_address;
    }

    /**
     * @param string|null $work_address
     */
    public function setWorkAddress(?string $work_address): void
    {
        $this->work_address = $work_address;
    }

    /**
     * @return mixed
     */
    public function getPracticeCertificate()
    {
        return $this->practice_certificate;
    }

    /**
     * @param mixed $practice_certificate
     */
    public function setPracticeCertificate($practice_certificate): void
    {
        $this->practice_certificate = Functions::StoreImage('practice_certificate','lawyer');
    }

    /**
     * @return string|null
     */
    public function getContactEmail(): ?string
    {
        return $this->contact_email;
    }

    /**
     * @param string|null $contact_email
     */
    public function setContactEmail(?string $contact_email): void
    {
        $this->contact_email = $contact_email;
    }

    /**
     * @return string|null
     */
    public function getContactMobile(): ?string
    {
        return $this->contact_mobile;
    }

    /**
     * @param string|null $contact_mobile
     */
    public function setContactMobile(?string $contact_mobile): void
    {
        $this->contact_mobile = $contact_mobile;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }


}
