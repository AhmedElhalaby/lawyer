<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer id
 * @property integer order_id
 * @method static find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class CompleteRequest extends Model
{
    protected $table = 'complete_requests';
    protected $fillable = ['order_id'];

    /**
     * @return BelongsTo
     */
    public function order(){
        return $this->belongsTo(Order::class);
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->order_id;
    }

    /**
     * @param int $order_id
     */
    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

}
