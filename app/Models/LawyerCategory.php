<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer id
 * @property integer lawyer_id
 * @property integer category_id
 * @method static find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class LawyerCategory extends Model
{
    protected $table = 'lawyer_categories';
    protected $fillable = ['lawyer_id','category_id'];

    /**
     * @return BelongsTo
     */
    public function lawyer(){
        return $this->belongsTo(Lawyer::class);
    }

    /**
     * @return BelongsTo
     */
    public function category(){
        return $this->belongsTo(Category::class);
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getLawyerId(): int
    {
        return $this->lawyer_id;
    }

    /**
     * @param int $lawyer_id
     */
    public function setLawyerId(int $lawyer_id): void
    {
        $this->lawyer_id = $lawyer_id;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

}
