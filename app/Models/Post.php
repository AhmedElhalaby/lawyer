<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Request;

/**
 * @property integer id
 * @property integer category_id
 * @property integer added_by
 * @property string|null title
 * @property string|null summery
 * @property string|null description
 * @property string|null title_ar
 * @property string|null summery_ar
 * @property string|null description_ar
 * @property string|null file
 * @property integer type
 * @property boolean is_active
 * @method static find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 */
class Post extends Model
{
    protected $table = 'posts';
    protected $fillable = ['category_id','added_by','title','summery','description','file','type','title_ar','summery_ar','description_ar','is_active'];

    /**
     * @return BelongsTo
     */
    public function category(){
        return $this->belongsTo(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function author(){
        return $this->belongsTo(Admin::class,'added_by','id');
    }
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {
        return $this->category_id;
    }

    /**
     * @param int $category_id
     */
    public function setCategoryId(int $category_id): void
    {
        $this->category_id = $category_id;
    }

    /**
     * @return int
     */
    public function getAddedBy(): int
    {
        return $this->added_by;
    }

    /**
     * @param int $added_by
     */
    public function setAddedBy(int $added_by): void
    {
        $this->added_by = $added_by;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getSummery(): ?string
    {
        return $this->summery;
    }

    /**
     * @param string|null $summery
     */
    public function setSummery(?string $summery): void
    {
        $this->summery = $summery;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string|null
     */
    public function getTitleAr(): ?string
    {
        return $this->title_ar;
    }

    /**
     * @param string|null $title_ar
     */
    public function setTitleAr(?string $title_ar): void
    {
        $this->title_ar = $title_ar;
    }

    /**
     * @return string|null
     */
    public function getSummeryAr(): ?string
    {
        return $this->summery_ar;
    }

    /**
     * @param string|null $summery_ar
     */
    public function setSummeryAr(?string $summery_ar): void
    {
        $this->summery_ar = $summery_ar;
    }

    /**
     * @return string|null
     */
    public function getDescriptionAr(): ?string
    {
        return $this->description_ar;
    }

    /**
     * @param string|null $description_ar
     */
    public function setDescriptionAr(?string $description_ar): void
    {
        $this->description_ar = $description_ar;
    }

    /**
     * @return string|null
     */
    public function getFile(): ?string
    {
        return $this->file;
    }

    /**
     * @param string|null $file
     */
    public function setFile(?string $file): void
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isIsActive(): bool
    {
        return $this->is_active;
    }

    /**
     * @param bool $is_active
     */
    public function setIsActive(bool $is_active): void
    {
        $this->is_active = $is_active;
    }
}
