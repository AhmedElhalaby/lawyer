<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property integer id
 * @property integer lawyer_id
 * @property integer order_id
 * @property integer order_time_id
 * @property integer order_date_id
 * @property float price
 * @property string|null note
 * @property integer status
 * @method OrderOffer find(int $id)
 * @method static updateOrCreate(array $array, array $array1)
 * @method OrderOffer where(string $string, int $getId)
 * @method OrderOffer first()
 */
class OrderOffer extends Model
{
    protected $table = 'order_offers';
    protected $fillable = ['lawyer_id','order_id','order_time_id','order_date_id','price','note','status'];

    /**
     * @return BelongsTo
     */
    public function lawyer(){
        return $this->belongsTo(Lawyer::class);
    }

    /**
     * @return BelongsTo
     */
    public function order(){
        return $this->belongsTo(Order::class);
    }

    /**
     * @return BelongsTo
     */
    public function order_time(){
        return $this->belongsTo(OrderTime::class)->with('time');
    }

    /**
     * @return BelongsTo
     */
    public function order_date(){
        return $this->belongsTo(OrderDate::class);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getLawyerId(): int
    {
        return $this->lawyer_id;
    }

    /**
     * @param int $lawyer_id
     */
    public function setLawyerId(int $lawyer_id): void
    {
        $this->lawyer_id = $lawyer_id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->order_id;
    }

    /**
     * @param int $order_id
     */
    public function setOrderId(int $order_id): void
    {
        $this->order_id = $order_id;
    }

    /**
     * @return int
     */
    public function getOrderTimeId(): int
    {
        return $this->order_time_id;
    }

    /**
     * @param int $order_time_id
     */
    public function setOrderTimeId(int $order_time_id): void
    {
        $this->order_time_id = $order_time_id;
    }

    /**
     * @return int
     */
    public function getOrderDateId(): int
    {
        return $this->order_date_id;
    }

    /**
     * @param int $order_date_id
     */
    public function setOrderDateId(int $order_date_id): void
    {
        $this->order_date_id = $order_date_id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string|null
     */
    public function getNote(): ?string
    {
        return $this->note;
    }

    /**
     * @param string|null $note
     */
    public function setNote(?string $note): void
    {
        $this->note = $note;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
