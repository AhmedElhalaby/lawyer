<?php


namespace App\Helpers;


class Constant
{
    const NOTIFICATION_TYPE = [
        'General'=>1,
        'Order'=>2,
        'NewMessage'=>2,
        'Ticket'=>3
    ];
    const USER_TYPE = [
        'Customer'=>1,
        'Lawyer'=>2
    ];
    const USER_TYPE_RULES = '1,2';

    const USER_GENDER = [
        'Male'=>1,
        'Female'=>2
    ];
    const USER_GENDER_RULES = '1,2';

    public static function EDUCATIONAL_LEVEL(){
        return [
            [
                'id'=>1,
                'name'=>__('crud.Lawyer.EducationalLevel.1')
            ],
            [
                'id'=>2,
                'name'=>__('crud.Lawyer.EducationalLevel.2')
            ],
            [
                'id'=>3,
                'name'=>__('crud.Lawyer.EducationalLevel.3')
            ],
        ];
    }
    const POST_TYPE = [
        'Post'=>1,
        'Question'=>2,
        'Video'=>3
    ];


    const ORDER_STATUS = [
        'WaitingOffers'=>0,
        'SelectingOffer'=>1,
        'Cancel'=>2,
        'Processing'=>3,
        'Completed'=>4,
        'WaitingClientEndApprove'=>5,
    ];
    const ORDER_STATUS_RULES = '1,2,3,4,5';
    const ORDER_OFFER_STATUS = [
        'Pending'=>0,
        'Approved'=>1,
        'Rejected'=>2,
    ];
    const LAWYER_STATUS = [
        'Pending'=>0,
        'Activated'=>1,
        'Suspended'=>2,
    ];
    const LAWYER_STATUS_ALLOWED = [
        self::LAWYER_STATUS['Activated']
    ];

    const TRANSACTION_TYPES = [
        'Deposit'=>1,
        'Withdraw'=>2,
        'Hold'=>3
    ];
    const TRANSACTION_STATUS = [
        'Pending'=>1,
        'Paid'=>2
    ];
    const TICKETS_STATUS = [
        'New'=>1,
        'Closed'=>2
    ];

    const SENDER_TYPE = [
        'User'=>1,
        'Admin'=>2,
    ];
}
