<?php


namespace App\Helpers;


use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderOffer;
use App\Models\PasswordReset;
use App\Models\Transaction;
use App\Models\VerifyAccounts;
use App\Notifications\PasswordReset as PasswordResetNotification;
use App\Notifications\VerifyAccount;
use App\Traits\ResponseTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Str;

class Functions
{
    use ResponseTrait;
    public static function SendNotification($user,$title,$msg,$title_ar,$msg_ar,$ref_id = null,$type= 0,$store = true,$replace =[])
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $registrationIds = $user->device_token;

        $message = array
        (
            'body'  => $msg_ar,
            'title' => $title_ar,
            'sound' => true,
        );
        $extraNotificationData = ["ref_id" =>$ref_id,"type"=>$type];
        $fields = array
        (
            'to'        => $registrationIds,
            'notification'  => $message,
            'data' => $extraNotificationData
        );
        $headers = array
        (
            'Authorization: key='.config('app.notification_key') ,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        if($store){
            $notify = new Notification();
            $notify->setType($type);
            $notify->setUserId($user->id);
            $notify->setTitle($title);
            $notify->setMessage($msg);
            $notify->setTitleAr($title_ar);
            $notify->setMessageAr($msg_ar);
            $notify->setRefId(@$ref_id);
            $notify->save();
        }
        return true;
    }
    public static function SendNotifications($users,$title,$msg,$ref_id = null,$type= 0,$store = true,$replace =[])
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $registrationIds = [];
        foreach ($users as $user){
            $registrationIds[] = $user->device_token;

        }

        $message = array
        (
            'body'  => $msg,
            'title' => $title,
            'sound' => true,
        );
        $extraNotificationData = ["ref_id" =>$ref_id,"type"=>$type];
        $fields = array
        (
            'registration_ids' => $registrationIds,
            'notification' => $message,
            'data' => $extraNotificationData
        );
        $headers = array
        (
            'Authorization: key='.config('app.notification_key') ,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        if($store){
            foreach ($users as $user){
                $notify = new Notification();
                $notify->setType($type);
                $notify->setUserId($user->id);
                $notify->setTitle($title);
                $notify->setMessage($msg);
                $notify->setTitleAr($title);
                $notify->setMessageAr($msg);
                $notify->setRefId(@$ref_id);
                $notify->save();
            }
        }
        return true;
    }
    public static function SendSms($msg,$to){
        $ch = curl_init();
        $userid = 'RUFAAGA@GMAIL.COM';
        $password = '12344321';
        $sender = 'RUFAQAA';
        $text = urlencode($msg);
        $encoding = 'UTF8';
        // auth call
        $url = "http://api.unifonic.com/wrapper/sendSMS.php?userid={$userid}&password={$password}&to={$to}&msg={$text}&sender={$sender}&encoding={$encoding}";
        $ret  = json_decode(file_get_contents($url), true);
        $response = curl_exec($ch);
        curl_close($ch);
    }
    public static function SendVerification($user,$type = null){
        $code = rand( 10000 , 99999 );
        $token = Str::random(40).time();
        VerifyAccounts::updateOrCreate(
            ['user_id' => $user->getId()],
            [
                'user_id' => $user->getId(),
                'code' => $code,
                'token' => $token,
            ]
        );
        static::SendSms('كود تفعيل الحساب هو : '.$code,$user->getMobile());
        $user->notify(
            new VerifyAccount($token,$code)
        );
    }
    public static function SendForget($user,$type = null){
        $code = rand( 10000 , 99999 );
        $token = Str::random(40).time();
        PasswordReset::updateOrCreate(
            ['user_id' => $user->getId()],
            [
                'user_id' => $user->getId(),
                'code' => $code,
                'token' => $token,
            ]
        );
        static::SendSms('كود استرجاع كلمة المرور هو : '.$code,$user->getMobile());
        $user->notify(
            new PasswordResetNotification($code)
        );
    }
    public static function StoreImage($attribute_name,$destination_path){
        $destination_path = "storage/".$destination_path.'/';
        $request = Request::instance();
        if ($request->hasFile($attribute_name)) {
            $file = $request->file($attribute_name);
            if ($file->isValid()) {
                $file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                $file->move($destination_path, $file_name);
                $attribute_value =  $destination_path.$file_name;
            }
        }
        return $attribute_value??null;
    }
    public static function StoreImageModel($file,$destination_path){
        $destination_path = "storage/".$destination_path.'/';
        if ($file->isValid()) {
            $file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            $file->move($destination_path, $file_name);
            $attribute_value =  $destination_path.$file_name;
        }
        return $attribute_value??null;
    }
    public static function CheckOrderSequence($Order,$Status){
        switch ($Status){
            case Constant::ORDER_STATUS['SelectingOffer']:{
                if ($Order->getStatus() != Constant::ORDER_STATUS['WaitingOffers']){
                    return (new Functions)->failJsonResponse([__('messages.wrong_sequence')]);
                }
                break;
            }
        }
    }
    public static function UserBalance($user_id){
        $Deposits = Transaction::where('user_id',$user_id)->where('type',Constant::TRANSACTION_TYPES['Deposit'])->sum('value');
        $Withdraws = Transaction::where('user_id',$user_id)->where('type',Constant::TRANSACTION_TYPES['Withdraw'])->sum('value');
        return $Deposits - $Withdraws;
    }
    public static function GenerateCheckout($value){
        $url = "https://test.oppwa.com/v1/checkouts";
        $data = "entityId=8a8294174d0595bb014d05d82e5b01d2" .
            "&amount=".$value .
            "&currency=USD" .
            "&paymentType=DB" .
            "&notificationUrl=http://www.example.com/notify";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        $responseData = json_decode($responseData);
        if ($responseData->result->code == "000.200.100"){
            return  [
                'status'=>true,
                'id'=>$responseData->id
            ];
        }else{
            return  [
                'status'=>false,
                'message'=>$responseData->result->description
            ];
        }
    }
    public static function CheckPayment($id){
        $url = "https://test.oppwa.com/v1/checkouts/{$id}/payment";
        $url .= "?entityId=8a8294174d0595bb014d05d82e5b01d2";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGE4Mjk0MTc0ZDA1OTViYjAxNGQwNWQ4MjllNzAxZDF8OVRuSlBjMm45aA=='));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        $responseData = json_decode($responseData);
        if($responseData->result->code == "000.100.110"){
            return true;
        }else{
            return false;
        }
//        if($responseData->result->code == "000.200.000"){
//            return false;
//        }
    }
    public static function OrderTimeNotification(){
        $Orders = Order::where('is_notified',false)->where('status',Constant::ORDER_STATUS['Processing'])->where('order_date',Carbon::today()->format('Y-m-d'))->where('order_time','>=',Carbon::now()->subMinutes(15))->get();
        foreach ($Orders as $Order){
            $Lawyer = $Order->lawyer->user;
            $User = $Order->user;
            Functions::SendNotification($Lawyer,'You have an appointment today','You have an appointment today at '.$Order->getOrderTime().' with '.$User->name.'','لديك موعد اليوم',' لديك موعد اليوم الساعة '.$Order->getOrderTime().' مع '.$User->name.'',$Order->getId(),Constant::NOTIFICATION_TYPE['Order']);
            Functions::SendNotification($User,'You have an appointment today','You have an appointment today at '.$Order->getOrderTime().' with '.$Lawyer->name.'','لديك موعد اليوم',' لديك موعد اليوم الساعة '.$Order->getOrderTime().' مع '.$Lawyer->name.'',$Order->getId(),Constant::NOTIFICATION_TYPE['Order']);
            $Order->setIsNotified(true);
            $Order->save();
        }
    }
    public static function OrderAutoComplete(){
        $Orders = Order::where('status',Constant::ORDER_STATUS['WaitingClientEndApprove'])->where('updated_at','>=',Carbon::now()->subDays(2))->get();
        foreach ($Orders as $Order){
            $Order->setStatus(Constant::ORDER_STATUS['Completed']);
            $Order->save();
            Functions::SendNotification($Order->lawyer->user,'Order Completed','Your Order has been completed automatically !','تم انهاء الطلب','لقد تم انهاء الطلب بشكل تلقائي',$Order->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
            Functions::SendNotification($Order->user,'Order Completed','Your Order has been completed automatically !','تم انهاء الطلب','لقد تم انهاء الطلب بشكل تلقائي',$Order->getId(),Constant::NOTIFICATION_TYPE['Order'],true);
        }
    }
    public static function TransactionReleaseHolding(){
        $Transactions = Transaction::where('type',Constant::TRANSACTION_TYPES['Hold'])->where('created_at','>=',Carbon::now()->subDay(10))->get();
        foreach ($Transactions as $transaction){
            $transaction->setType(Constant::TRANSACTION_TYPES['Deposit']);
            $transaction->save();
        }
    }
    public static function TotalProfit(){
        $Orders = Order::where('status',Constant::ORDER_STATUS['Completed'])->pluck('order_offer_id');
        $Value = OrderOffer::whereIn('id',$Orders)->sum('price');
        return $Value*25/100;
    }
    public static function TotalLawyerProfit(){
        $Orders = Order::where('status',Constant::ORDER_STATUS['Completed'])->pluck('order_offer_id');
        $Value = OrderOffer::whereIn('id',$Orders)->sum('price');
        return $Value*75/100;
    }
}
